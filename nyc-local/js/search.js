const app = new Vue ({
    el: '#app',
    delimiters: ['${', '}'],
    data: function() {
        return {
            search: '',
            "participants": [
             {
               "First Name": "Mutlaq",
               "Last Name": "Almorished",
               "Job Title": "CEO",
               "Company Name": "Tasnee",
               "Headshot Upload": "nyc-mutlaq-almorished.jpg"
             },
             {
               "First Name": "Charlie",
               "Last Name": "Alutto",
               "Job Title": "CEO",
               "Company Name": "Stericycle",
               "Headshot Upload": "nyc-charlie-alutto.jpg",
               "Guest": "Jorie Alutto"
             },
             {
               "First Name": "Astrid",
               "Last Name": "Alvarez",
               "Job Title": "CEO",
               "Company Name": "Grupo Energía Bogotá",
               "Headshot Upload": "nyc-astrid-alvarez.jpg",
               "Guest": "Johanna Perilla"
             },
             {
               "First Name": "Tarek",
               "Last Name": "Amer",
               "Job Title": "Governor",
               "Company Name": "Central Bank of Egypt (CBE)",
               "Headshot Upload": "nyc-tarek-amer.jpg"
             },
             {
               "First Name": "Marco",
               "Last Name": "Annunziata",
               "Job Title": "Co-Founder",
               "Company Name": "Annunziata \+ Desai Advisors",
               "Headshot Upload": "nyc-marco-annunziata.jpg"
             },
             {
               "First Name": "David",
               "Last Name": "Anton",
               "Job Title": "President",
               "Company Name": "Anton & Partners Inc.",
               "Headshot Upload": "nyc-david-anton.jpg",
               "Guest": "Neva Anton"
             },
             {
               "First Name": "Zain",
               "Last Name": "Asher",
               "Job Title": "Anchor",
               "Company Name": "CNN International",
               "Headshot Upload": "nyc-zain-asher.jpg",
               "Guest": "Steve Peoples"
             },
             {
               "First Name": "Howard",
               "Last Name": "Belk",
               "Job Title": "Co-CEO and Chief Creative Officer",
               "Company Name": "Siegel\+Gale",
               "Headshot Upload": "nyc-howard-belk.jpg",
               "Guest": "Carrie Belk"
             },
             {
               "First Name": "Gary",
               "Last Name": "Bettman",
               "Job Title": "Commissioner",
               "Company Name": "National Hockey League",
               "Headshot Upload": "nyc-gary-bettman.jpg"
             },
             {
               "First Name": "Frank",
               "Last Name": "Bisignano",
               "Job Title": "Chairman and CEO",
               "Company Name": "First Data Corporation",
               "Headshot Upload": "nyc-frank-bisignano.jpg",
               "Guest": "Tracy Bisignano"
             },
             {
               "First Name": "Lloyd",
               "Last Name": "Blankfein",
               "Job Title": "Chairman of the Board",
               "Company Name": "Goldman Sachs",
               "Headshot Upload": "nyc-lloyd-blankfein.jpg",
               "Guest": "Laura Blankfein"
             },
             {
               "First Name": "Rachel",
               "Last Name": "Blumenthal",
               "Job Title": "Founder and CEO",
               "Company Name": "Rockets of Awesome",
               "Headshot Upload": "nyc-rachel-blumenthal.jpg",
               "Guest": "Neil Blumenthal"
             },
             {
               "First Name": "Dave",
               "Last Name": "Brandon",
               "Job Title": "Chairman, Domino's Pizza, Inc.",
               "Company Name": "Chairman, Toys\"R\"Us, Inc.",
               "Headshot Upload": "nyc-dave-brandon.jpg",
               "Guest": "Jan Brandon"
             },
             {
               "First Name": "Thierry",
               "Last Name": "Breton",
               "Job Title": "Chairman and CEO",
               "Company Name": "Atos",
               "Headshot Upload": "nyc-thierry-breton.jpg"
             },
             {
               "First Name": "Tina",
               "Last Name": "Brown",
               "Job Title": "Founder and CEO",
               "Company Name": "Tina Brown Live Media / Women in the World",
               "Headshot Upload": "nyc-tina-brown.jpg"
             },
             {
               "First Name": "Zak",
               "Last Name": "Brown",
               "Job Title": "CEO",
               "Company Name": "McLaren Racing Ltd",
               "Headshot Upload": "nyc-zak-brown.jpg"
             },
             {
               "First Name": "George",
               "Last Name": "W. Bush",
               "Job Title": "43rd President of the United States",
               "Company Name": "",
               "Headshot Upload": "nyc-george-bush.jpg"
             },
             {
               "First Name": "Bernard",
               "Last Name": "Byrne",
               "Job Title": "CEO",
               "Company Name": "AIB",
               "Headshot Upload": "nyc-bernard-byrne.jpg",
               "Guest": "Fiona Byrne"
             },
             {
               "First Name": "Steve",
               "Last Name": "Cahillane",
               "Job Title": "Chairman and CEO",
               "Company Name": "Kellogg Company",
               "Headshot Upload": "nyc-steve-cahillane.jpg",
               "Guest": "Tracy Cahillane"
             },
             {
               "First Name": "Bill",
               "Last Name": "Clinton",
               "Job Title": "42nd President of the United States",
               "Company Name": "",
               "Headshot Upload": "nyc-bill-clinton.jpg"
             },
             {
               "First Name": "Steven",
               "Last Name": "Collis",
               "Job Title": "CEO",
               "Company Name": "AmerisourceBergen Corporation",
               "Headshot Upload": "nyc-steven-collis.jpg",
               "Guest": "Erin Miller"
             },
             {
               "First Name": "Marlene",
               "Last Name": "Colucci-Renna",
               "Job Title": "Executive Director",
               "Company Name": "The Business Council",
               "Headshot Upload": "nyc-marlene-colucci-renna.jpg",
               "Guest": "Stephen Renna"
             },
             {
               "First Name": "Maurice",
               "Last Name": "Conti",
               "Job Title": "Chief Innovation Officer",
               "Company Name": "Telefónica Alpha",
               "Headshot Upload": "nyc-maurice-conti.jpg"
             },
             {
               "First Name": "Sarah",
               "Last Name": "Davis",
               "Job Title": "President",
               "Company Name": "Loblaw Companies",
               "Headshot Upload": "nyc-sarah-davis.jpg",
               "Guest": "Gary Davis"
             },
             {
               "First Name": "Larry",
               "Last Name": "De Shon",
               "Job Title": "President and CEO",
               "Company Name": "Avis Budget Group",
               "Headshot Upload": "nyc-larry-de_shon.jpg",
               "Guest": "Gail De Shon"
             },
             {
               "First Name": "Johan",
               "Last Name": "Dennelind",
               "Job Title": "CEO",
               "Company Name": "Telia Company",
               "Headshot Upload": "nyc-johan-dennelind.jpg"
             },
             {
               "First Name": "Brad",
               "Last Name": "Dickerson",
               "Job Title": "CEO",
               "Company Name": "Blue Apron",
               "Headshot Upload": "nyc-brad-dickerson.jpg",
               "Guest": "Yelena Dickerson"
             },
             {
               "First Name": "John",
               "Last Name": "Donahoe",
               "Job Title": "President and CEO",
               "Company Name": "ServiceNow",
               "Headshot Upload": "nyc-john-donahoe.jpg"
             },
             {
               "First Name": "Arnold W.",
               "Last Name": "Donald",
               "Job Title": "President and CEO",
               "Company Name": "Carnival Corporation",
               "Headshot Upload": "nyc-arnold-donald.jpg"
             },
             {
               "First Name": "Francisco",
               "Last Name": "D'Souza",
               "Job Title": "Vice Chairman and CEO",
               "Company Name": "Cognizant",
               "Headshot Upload": "nyc-francisco-dsouza.jpg",
               "Guest": "Dr. Maria Ines Kavamura"
             },
             {
               "First Name": "Henrik",
               "Last Name": "Ehrnrooth",
               "Job Title": "President and CEO",
               "Company Name": "KONE Corporation",
               "Headshot Upload": "nyc-henrik-ehrnrooth.jpg",
               "Guest": "Johanna Ehrnrooth"
             },
             {
               "First Name": "Brad",
               "Last Name": "Feldmann",
               "Job Title": "President and CEO",
               "Company Name": "Cubic Corporation",
               "Headshot Upload": "nyc-bradley-feldmann.jpg",
               "Guest": "Debora Feldmann"
             },
             {
               "First Name": "Michael",
               "Last Name": "Field",
               "Job Title": "CEO",
               "Company Name": "The Raymond Corporation",
               "Headshot Upload": "nyc-michael-field.jpg",
               "Guest": "Robin Tobin"
             },
             {
               "First Name": "Peter",
               "Last Name": "Fox",
               "Job Title": "Executive Chairman",
               "Company Name": "Linfox",
               "Headshot Upload": "nyc-peter-fox.jpg"
             },
             {
               "First Name": "Simon",
               "Last Name": "Fuller",
               "Job Title": "Founder and CEO",
               "Company Name": "XIX Entertainment",
               "Headshot Upload": "nyc-simon-fuller.jpg",
               "Guest": "Savannah Wesley"
             },
             {
               "First Name": "Emerson U.",
               "Last Name": "Fullwood",
               "Job Title": "",
               "Company Name": "Xerox (Former)",
               "Headshot Upload": "nyc-emerson-fullwood.jpg",
               "Guest": "Vernita Fullwood"
             },
             {
               "First Name": "Gary",
               "Last Name": "Gauba",
               "Job Title": "Founder and Managing Director",
               "Company Name": "The CXO Fund",
               "Headshot Upload": "nyc-gary-gauba.jpg",
               "Guest": "Pooja Gauba"
             },
             {
               "First Name": "Alexis",
               "Last Name": "Glick",
               "Job Title": "CEO",
               "Company Name": "GENYOUth Foundation",
               "Headshot Upload": "nyc-alexis-glick.jpg"
             },
             {
               "First Name": "Carl",
               "Last Name": "Grivner",
               "Job Title": "CEO",
               "Company Name": "Colt Technology Services",
               "Headshot Upload": "nyc-carl-grivner.jpg",
               "Guest": "Kaye Phares"
             },
             {
               "First Name": "Desiree",
               "Last Name": "Gruber",
               "Job Title": "CEO",
               "Company Name": "Full Picture",
               "Headshot Upload": "nyc-desiree-gruber.jpg",
               "Guest": "Kyle MacLachlan"
             },
             {
               "First Name": "Al",
               "Last Name": "Guido",
               "Job Title": "President",
               "Company Name": "San Francisco 49ers",
               "Headshot Upload": "nyc-al-guido.jpg",
               "Guest": "Thea Guido"
             },
             {
               "First Name": "Sanjay",
               "Last Name": "Gupta, M.D.",
               "Job Title": "CNN Chief Medical Correspondent",
               "Company Name": "Faculty Neurosurgeon, The Emory Clinic",
               "Headshot Upload": "nyc-sanjay-gupta.jpg"
             },
             {
               "First Name": "Mikko",
               "Last Name": "Helander",
               "Job Title": "President and CEO",
               "Company Name": "Kesko Corporation",
               "Headshot Upload": "nyc-mikko-helander.jpg",
               "Guest": "Anne Helander"
             },
             {
               "First Name": "Andres",
               "Last Name": "Holzer",
               "Job Title": "Vice Chairman",
               "Company Name": "Dufry",
               "Headshot Upload": "nyc-andres-holzer.jpg",
               "Guest": "Christine Martínez"
             },
             {
               "First Name": "Arianna",
               "Last Name": "Huffington",
               "Job Title": "Founder and CEO",
               "Company Name": "Thrive Global",
               "Headshot Upload": "nyc-arianna-huffington.jpg"
             },
             {
               "First Name": "Glenn",
               "Last Name": "Hutchins",
               "Job Title": "Chairman",
               "Company Name": "North Island",
               "Headshot Upload": "nyc-glenn-hutchins.jpg"
             },
             {
               "First Name": "Dev",
               "Last Name": "Ittycheria",
               "Job Title": "President & CEO",
               "Company Name": "MongoDB",
               "Headshot Upload": "nyc-dev-ittycheria.jpg",
               "Guest": "Dr. Anju Thomas"
             },
             {
               "First Name": "Rich",
               "Last Name": "Hume",
               "Job Title": "CEO",
               "Company Name": "Tech Data Corporation",
               "Headshot Upload": "nyc-rich-hume.jpg"
             },
             {
               "First Name": "Ralph",
               "Last Name": "Izzo",
               "Job Title": "Chairman of the Board, President and CEO",
               "Company Name": "Public Service Enterprise Group Incorporated",
               "Headshot Upload": "nyc-ralph-izzo.jpg",
               "Guest": "Karen Izzo"
             },
             {
               "First Name": "Pulkit",
               "Last Name": "Jaiswal",
               "Job Title": "Co-founder and CEO",
               "Company Name": "SwarmX",
               "Headshot Upload": "nyc-pulkit-jaiswal.jpg",
               "Guest": "Coco Mercy Chu"
             },
             {
               "First Name": "Ken",
               "Last Name": "Jarin",
               "Job Title": "Partner",
               "Company Name": "Ballard Spahr.",
               "Headshot Upload": "nyc-ken-jarin.jpg",
               "Guest": "Robin Wiessmann"
             },
             {
               "First Name": "Jo Ann",
               "Last Name": "Jenkins",
               "Job Title": "CEO",
               "Company Name": "AARP",
               "Headshot Upload": "nyc-joann-jenkins.jpg"
             },
             {
               "First Name": "Khaled",
               "Last Name": "Juffali",
               "Job Title": "Chairman and CEO",
               "Company Name": "Khaled Juffali Company",
               "Headshot Upload": "nyc-khaled-juffali.jpg",
               "Guest": "Olfat Juffali"
             },
             {
               "First Name": "Shelly",
               "Last Name": "Kapoor Collins",
               "Job Title": "General Partner",
               "Company Name": "Shatter Fund",
               "Headshot Upload": "nyc-shelly-kapoor-collins.jpg",
               "Guest": "Patrick Collins"
             },
             {
               "First Name": "Brad",
               "Last Name": "Katsuyama",
               "Job Title": "Co-Founder and CEO",
               "Company Name": "IEX Group, Inc.",
               "Headshot Upload": "nyc-brad-katsuyama.jpg",
               "Guest": "Ashley Katsuyama"
             },
             {
               "First Name": "Brad",
               "Last Name": "Keywell",
               "Job Title": "Founder and CEO",
               "Company Name": "Uptake",
               "Headshot Upload": "nyc-brad-keywell.jpg",
               "Guest": "Kim Keywell"
             },
             {
               "First Name": "Keith",
               "Last Name": "Krach",
               "Job Title": "Chairman of the Board",
               "Company Name": "DocuSign",
               "Headshot Upload": "nyc-keith-krach.jpg",
               "Guest": "Metta Krach"
             },
             {
               "First Name": "Howard",
               "Last Name": "Krein, M.D., PhD",
               "Job Title": "Chief Medical Officer",
               "Company Name": "StartUp Health",
               "Headshot Upload": "nyc-howard-krein.jpg"
             },
             {
               "First Name": "Steven",
               "Last Name": "Krein",
               "Job Title": "CEO and Co-founder",
               "Company Name": "StartUp Health",
               "Headshot Upload": "nyc-steven-krein.jpg"
             },
             {
               "First Name": "Ken",
               "Last Name": "Lamneck",
               "Job Title": "President and CEO",
               "Company Name": "Insight",
               "Headshot Upload": "nyc-ken-lamneck.jpg",
               "Guest": "Marianne Lamneck"
             },
             {
               "First Name": "David",
               "Last Name": "Larabell",
               "Job Title": "Literary Agent",
               "Company Name": "Creative Artists Agency",
               "Headshot Upload": "nyc-david-larabell.jpg",
               "Guest": "Caroline Larabell"
             },
             {
               "First Name": "Kim",
               "Last Name": "Le",
               "Job Title": "CEO",
               "Company Name": "A2Q2",
               "Headshot Upload": "nyc-kim-le.jpg"
             },
             {
               "First Name": "Donald E.",
               "Last Name": "Leighton",
               "Job Title": "Pastor",
               "Company Name": "St. John Vianney Church",
               "Headshot Upload": "nyc-donald-leighton.jpg",
               "Guest": "Carol Curley"
             },
             {
               "First Name": "Rich",
               "Last Name": "Lesser",
               "Job Title": "CEO",
               "Company Name": "The Boston Consulting Group",
               "Headshot Upload": "nyc-rich-lesser.jpg",
               "Guest": "Gabrielle Lesser"
             },
             {
               "First Name": "David",
               "Last Name": "Levy",
               "Job Title": "President",
               "Company Name": "Turner",
               "Headshot Upload": "nyc-david-levy.jpg",
               "Guest": "Niki Levy"
             },
             {
               "First Name": "William",
               "Last Name": "Lewis",
               "Job Title": "CEO and Publisher, The Wall Street Journal",
               "Company Name": "Dow Jones",
               "Headshot Upload": "nyc-william-lewis.jpg"
             },
             {
               "First Name": "Peter",
               "Last Name": "Martyr",
               "Job Title": "Global Chief Executive",
               "Company Name": "Norton Rose Fulbright",
               "Headshot Upload": "nyc-peter-martyr.jpg"
             },
             {
                "First Name": "Bill ",
                "Last Name": "McDermott",
                "Job Title": "CEO",
                "Company Name": "SAP",
                "Headshot Upload": "nyc-bill-mcdermott.jpg",
               "Guest": "Julie McDermott"
              },
             {
               "First Name": "William",
               "Last Name": "Meaney",
               "Job Title": "President and CEO",
               "Company Name": "Iron Mountain",
               "Headshot Upload": "nyc-bill-meaney.jpg",
               "Guest": "Judith Meaney"
             },
             {
               "First Name": "Manuel",
               "Last Name": "Medina",
               "Job Title": "CEO",
               "Company Name": "Cyxtera",
               "Headshot Upload": "nyc-manuel-medina.jpg",
               "Guest": "Stacey Magazanick"
             },
             {
               "First Name": "Ken",
               "Last Name": "Moelis",
               "Job Title": "Chairman and CEO",
               "Company Name": "Moelis & Company",
               "Headshot Upload": "nyc-ken-moelis.jpg"
             },
             {
               "First Name": "Julianne",
               "Last Name": "Moore",
               "Job Title": "Academy Award and Emmy Winning Actor",
               "Company Name": "",
               "Headshot Upload": "nyc-julianne-moore.jpg"
             },
             {
               "First Name": "Carlos",
               "Last Name": "Moreira",
               "Job Title": "Founder, Chairman and CEO",
               "Company Name": "Wisekey",
               "Headshot Upload": "nyc-carlos-moreira.jpg",
               "Guest": "Anne Feuardent Moreira"
             },
             {
               "First Name": "Bob",
               "Last Name": "Moritz",
               "Job Title": "Global Chairman",
               "Company Name": "PwC",
               "Headshot Upload": "nyc-robert-moritz.jpg"
             },
             {
               "First Name": "Ravi Chandra",
               "Last Name": "Muddada",
               "Job Title": "Principal Finance Secretary",
               "Company Name": "Government of Andhra Pradesh",
               "Headshot Upload": "nyc-ravi-chandra.jpg",
               "Guest": "Sridhar Chimaladinne"
             },
             {
               "First Name": "Steve",
               "Last Name": "Nelson",
               "Job Title": "CEO",
               "Company Name": "UnitedHealthcare",
               "Headshot Upload": "nyc-steve-nelson.jpg"
             },
             {
               "First Name": "Trevor",
               "Last Name": "Noah",
               "Job Title": "Comedian and Host",
               "Company Name": "The Daily Show on Comedy Central",
               "Headshot Upload": "nyc-trevor-noah.jpg"
             },
             {
               "First Name": "Indra",
               "Last Name": "Nooyi",
               "Job Title": "Chairman",
               "Company Name": "PepsiCo",
               "Headshot Upload": "nyc-indra-nooyi.jpg",
               "Guest": "Raj Nooyi"
             },
             {
               "First Name": "Scott",
               "Last Name": "O'Neil",
               "Job Title": "CEO",
               "Company Name": "Harris Blitzer Sports & Entertainment",
               "Headshot Upload": "nyc-scott-oneil.jpg",
               "Guest": "Lisa O'Neil"
             },
             {
               "First Name": "Dinesh",
               "Last Name": "Paliwal",
               "Job Title": "President and CEO",
               "Company Name": "HARMAN",
               "Headshot Upload": "nyc-dinesh-paliwal.jpg",
               "Guest": "Ila Paliwal"
             },
             {
               "First Name": "Sam",
               "Last Name": "Palmisano",
               "Job Title": "Chairman",
               "Company Name": "The Center for Global Enterprise",
               "Headshot Upload": "nyc-sam-palmisano.jpg"
             },
             {
               "First Name": "Joseph",
               "Last Name": "Papa",
               "Job Title": "Chairman and CEO",
               "Company Name": "Bausch Health Companies Inc.",
               "Headshot Upload": "nyc-joseph-papa.jpg"
             },
             {
               "First Name": "Chemi",
               "Last Name": "Peres",
               "Job Title": "Chairman of the Board of Directors",
               "Company Name": "Peres Center for Peace and Innovation",
               "Headshot Upload": "nyc-chemi-peres.jpg",
               "Guest": "Gila Peres"
             },
             {
               "First Name": "Kevin",
               "Last Name": "Plank",
               "Job Title": "Chairman and CEO",
               "Company Name": "Under Armour",
               "Headshot Upload": "nyc-kevin-plank.jpg"
             },
             {
               "First Name": "Edmund",
               "Last Name": "Pribitkin",
               "Job Title": "Chief Medical Officer",
               "Company Name": "Thomas Jefferson University",
               "Headshot Upload": "nyc-edmund-pribitkin.jpg"
             },
             {
               "First Name": "Gary",
               "Last Name": "Pruitt",
               "Job Title": "President and CEO",
               "Company Name": "The Associated Press",
               "Headshot Upload": "nyc-gary-pruitt.jpg"
             },
             {
               "First Name": "Frank",
               "Last Name": "Quattrone",
               "Job Title": "Executive Chairman",
               "Company Name": "Qatalyst Partners",
               "Headshot Upload": "nyc-frank-quattrone.jpg"
             },
             {
               "First Name": "Marc",
               "Last Name": "Raibert",
               "Job Title": "CEO",
               "Company Name": "Boston Dynamics",
               "Headshot Upload": "nyc-marc-raibert.jpg"
             },
             {
               "First Name": "Hernán",
               "Last Name": "Rincón",
               "Job Title": "CEO",
               "Company Name": "Avianca",
               "Headshot Upload": "nyc-hernan-rincon.jpg",
               "Guest": "Elvira Garcia"
             },
             {
               "First Name": "Liz",
               "Last Name": "Robbins",
               "Job Title": "Owner/CEO",
               "Company Name": "Liz Robbins Associates",
               "Headshot Upload": "nyc-liz-robbins.jpg"
             },
             {
               "First Name": "Howie",
               "Last Name": "Roseman",
               "Job Title": "Executive Vice President of Football Operations",
               "Company Name": "Philadelphia Eagles",
               "Headshot Upload": "nyc-howie-roseman.jpg",
               "Guest": "Mindy Roseman"
             },
             {
               "First Name": "Stephen",
               "Last Name": "Ross",
               "Job Title": "Chairman and Founder",
               "Company Name": "Related Companies",
               "Headshot Upload": "nyc-stephen-ross.jpg",
               "Guest": "Kara Ross"
             },
             {
               "First Name": "Panu",
               "Last Name": "Routila",
               "Job Title": "President and CEO",
               "Company Name": "Konecranes Plc",
               "Headshot Upload": "nyc-panu-routila.jpg",
               "Guest": "Soile Routila"
             },
             {
               "First Name": "Toby",
               "Last Name": "Ruckert",
               "Job Title": "CEO",
               "Company Name": "UIB Holdings",
               "Headshot Upload": "nyc-toby-ruckert.jpg",
               "Guest": "Margit Brusda"
             },
             {
               "First Name": "Anthoni",
               "Last Name": "Salim",
               "Job Title": "President and CEO",
               "Company Name": "Salim Group",
               "Headshot Upload": "nyc-anthoni-salim.jpg"
             },
             {
               "First Name": "Ramin",
               "Last Name": "Sayar",
               "Job Title": "President and CEO",
               "Company Name": "Sumo Logic",
               "Headshot Upload": "nyc-ramin-sayar.jpg",
               "Guest": "Samantha Sayar"
             },
             {
               "First Name": "Geoff",
               "Last Name": "Scott",
               "Job Title": "CEO",
               "Company Name": "ASUG",
               "Headshot Upload": "nyc-geoff-scott.jpg",
               "Guest": "Jennifer Scott"
             },
             {
               "First Name": "Adam",
               "Last Name": "Silver",
               "Job Title": "Commissioner",
               "Company Name": "National Basketball Association",
               "Headshot Upload": "nyc-adam-silver.jpg",
               "Guest": "Maggie Grise"
             },
             {
               "First Name": "Randy",
               "Last Name": "Skoda",
               "Job Title": "President and CEO",
               "Company Name": "Topco Associates, LLC",
               "Headshot Upload": "nyc-randy-skoda.jpg",
               "Guest": "Julie Skoda"
             },
             {
               "First Name": "Aaron",
               "Last Name": "Sorkin",
               "Job Title": "Academy Award Winning Writer and Renowned Playwright",
               "Company Name": "",
               "Headshot Upload": "nyc-aaron-sorkin.jpg"
             },
             {
               "First Name": "Jim",
               "Last Name": "Spradlin",
               "Job Title": "CEO",
               "Company Name": "GROWMARK, Inc.",
               "Headshot Upload": "nyc-jim-spradlin.jpg",
               "Guest": "Lisbeth Spradlin"
             },
             {
               "First Name": "Mike",
               "Last Name": "Tannenbaum",
               "Job Title": "Executive Vice President of Football Operations",
               "Company Name": "Miami Dolphins, Ltd.",
               "Headshot Upload": "nyc-mike-tannenbaum.jpg",
               "Guest": "Michelle Tannenbaum"
             },
             {
               "First Name": "Mark",
               "Last Name": "Trudeau",
               "Job Title": "President and CEO",
               "Company Name": "Mallinckrodt Pharmaceuticals",
               "Headshot Upload": "nyc-mark-trudeau.jpg",
               "Guest": "Elizabeth Rodriguez"
             },
             {
               "First Name": "Joseph",
               "Last Name": "Tucci",
               "Job Title": "Chairman",
               "Company Name": "Bridge Growth Partners",
               "Headshot Upload": "nyc-joseph-tucci.jpg"
             },
             {
               "First Name": "Tim",
               "Last Name": "Tyson",
               "Job Title": "Chairman and CEO",
               "Company Name": "Avara Pharmaceutical Services",
               "Headshot Upload": "nyc-tim-tyson.jpg"
             },
             {
               "First Name": "Andreas",
               "Last Name": "Umbach",
               "Job Title": "Chairman",
               "Company Name": "Landis\+Gyr AG",
               "Headshot Upload": "nyc-andreas-umbach.jpg",
               "Guest": "Karen Umbach"
             },
             {
               "First Name": "Jacques",
               "Last Name": "van den Broek",
               "Job Title": "CEO and Chairman of the Executive Board",
               "Company Name": "Randstad Holding NV",
               "Headshot Upload": "nyc-jacques-vandenbroek.jpg",
               "Guest": "Mariëtte Turkenburg"
             },
             {
               "First Name": "Krishnadevarayalu",
               "Last Name": "Vasireddy",
               "Job Title": "CEO",
               "Company Name": "APCFSS, Government of Andhra Pradesh",
               "Headshot Upload": "nyc-krishna-vasireddy.jpg",
               "Guest": "Surendra Datti"
             },
             {
               "First Name": "Nick",
               "Last Name": "Vlahos",
               "Job Title": "CEO",
               "Company Name": "The Honest Company",
               "Headshot Upload": "nyc-nick-vlahos.jpg",
               "Guest": "Muhammad Shahzad"
             },
             {
               "First Name": "Padmasree",
               "Last Name": "Warrior",
               "Job Title": "CEO",
               "Company Name": "NIO U.S.",
               "Headshot Upload": "nyc-warrior-padmasree.jpg",
               "Guest": "Mohan Warrior"
             },
             {
               "First Name": "Sanford",
               "Last Name": "Weill",
               "Job Title": "CEO",
               "Company Name": "Casa Rosa Ventures, LLC",
               "Headshot Upload": "nyc-sanford-weill.jpg",
               "Guest": "Joan Weill"
             },
             {
               "First Name": "Mark",
               "Last Name": "Weinberger",
               "Job Title": "Global Chairman and CEO",
               "Company Name": "EY",
               "Headshot Upload": "nyc-mark-weinberger.jpg",
               "Guest": "Nancy Weinberger"
             },
             {
               "First Name": "Maggie",
               "Last Name": "Wilderotter",
               "Job Title": "Chairman and CEO",
               "Company Name": "Grand Reserve Inn",
               "Headshot Upload": "nyc-maggie-wilderotter.jpg",
               "Guest": "Andrea Doelling"
             },
             {
               "First Name": "Steve",
               "Last Name": "Williams",
               "Job Title": "President and CEO",
               "Company Name": "Suncor Energy",
               "Headshot Upload": "nyc-steve-williams.jpg",
               "Guest": "Mary Williams"
             },
             {
               "First Name": "John",
               "Last Name": "Wren",
               "Job Title": "President and CEO",
               "Company Name": "Omnicom",
               "Headshot Upload": "nyc-john-wren.jpg"
             },
             {
               "First Name": "Hal",
               "Last Name": "Yoh",
               "Job Title": "Chairman and CEO",
               "Company Name": "Day & Zimmermann",
               "Headshot Upload": "nyc-hal-yoh.jpg"
             },
             {
               "First Name": "Jed",
               "Last Name": "York",
               "Job Title": "CEO",
               "Company Name": "San Francisco 49ers",
               "Headshot Upload": "nyc-jed-york.jpg"
             }
          ]
        };
    },
    computed: {
        filteredList: function() {
            let parts_arr = this.participants,
                searchString = this.search;
            if(!searchString) {
                return parts_arr;
            }

            searchString = searchString.trim().toLowerCase();

            parts_arr = parts_arr.filter(function(part) {
                if(part['First Name'].toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part['Last Name'].toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part['Job Title'].toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part['Company Name'].toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                }
            })
            return parts_arr;
        }
    },
    methods: {
        imageLoadError : function(part) {
            // console.log(part);
            // this.src = './img/headshots/white-filler.jpg';
            //console.log('bilbo baggins');
        }
    }
});






