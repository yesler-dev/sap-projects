const app = new Vue({
  el: "#app",
  delimiters: ["${", "}"],
  data: function() {
    return {
      search: "",
      participants: [
        {
          isSpeaker: null,
          firstName: "David",
          lastName: "Abney",
          title: "Chairman and CEO",
          company: "UPS",
          headshot: "nyc-david-abney.jpg",
          guest: "Sherry Abney"
        },
        {
          isSpeaker: null,
          firstName: "Mutlaq",
          lastName: "Almorished",
          title: "CEO",
          company: "Tasnee",
          headshot: "nyc-mutlaq-almorished.jpg",
          guest: "Lama AlSaleh"
        },
        {
          isSpeaker: null,
          firstName: "David",
          lastName: "Anton",
          title: "President",
          company: "Anton & Partners",
          headshot: "nyc-david-anton.jpg",
          guest: "Neva Anton"
        },
        {
          isSpeaker: true,
          firstName: "Zain",
          lastName: "Asher",
          title: "Anchor",
          company: "CNN International",
          headshot: "nyc-zain-asher.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Caryn Seidman",
          lastName: "Becker",
          title: "Co-founder, Chairman, and CEO",
          company: "CLEAR",
          headshot: "nyc-caryn-seidman-becker.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Howard",
          lastName: "Belk",
          title: "Co-CEO, Chief Creative Officer",
          company: "Siegel+Gale",
          headshot: "nyc-howard-belk.jpg",
          guest: "Carrie Belk"
        },

        {
          isSpeaker: null,
          firstName: "Shlomi",
          lastName: "Ben Haim",
          title: "Co-founder and CEO",
          company: "JFrog",
          headshot: "nyc-shlomi-hen-haim.jpg",
          guest: "Dana Ben Haim"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Bettman",
          title: "Commissioner",
          company: "NHL",
          headshot: "nyc-gary-bettman.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Marco",
          lastName: "Bizzarri",
          title: "President and CEO",
          company: "Gucci",
          headshot: "nyc-marco-bizzarri.jpg",
          guest: "Maristella Levoni"
        },
        {
          isSpeaker: null,
          firstName: "Dominic",
          lastName: "Blakemore",
          title: "Group CEO",
          company: "Compass Group",
          headshot: "nyc-dominic-blakemore.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Aryeh B.",
          lastName: "Bourkoff",
          title: "Founder and CEO",
          company: "LionTree LLC",
          headshot: "nyc-aryeh-bourkoff.jpeg"
        },
        {
          isSpeaker: null,
          firstName: "Dave",
          lastName: "Brandon",
          title: "Chairman",
          company: "Domino's Pizza, Inc.",
          headshot: "nyc-dave-brandon.jpg",
          guest: "Jan Brandon"
        },
        {
          isSpeaker: null,
          firstName: "Jacques",
          lastName: "van den Broek",
          title: "CEO and Chairman of the Executive Board",
          company: "Randstad N.V.",
          headshot: "nyc-jacques-van-den-broek.jpg",
          guest: "Mariëtte Turkenburg"
        },
        {
          isSpeaker: null,
          firstName: "Tina",
          lastName: "Brown",
          title: "Founder and CEO",
          company: "Tina Brown Live Media/Women in the World",
          headshot: "nyc-tina-brown.jpg",
          guest: ""
        },
        {
          isSpeaker: null,
          firstName: "Levent",
          lastName: "Çakıroğlu",
          title: "CEO",
          company: "KOC",
          headshot: "nyc-levent-cakıroglu.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Cohn",
          title:
            "Former Director of the United States National Economic Council",
          company: "",
          headshot: "nyc-gary-cohn.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Alan",
          lastName: "Colberg",
          title: "President and CEO",
          company: "Assurant, Inc.",
          headshot: "nyc-alan-colberg.jpg",
          guest: "Li Colberg"
        },
        {
          isSpeaker: null,
          firstName: "Marlene",
          lastName: "Colucci",
          title: "Executive Director",
          company: "The Business Council",
          headshot: "nyc-marlene-colucci-renna.jpg",
          guest: "Stephen Renna"
        },
        {
          isSpeaker: null,
          firstName: "Maurice",
          lastName: "Conti",
          title: "CEO",
          company: "Applied Intelligence",
          headshot: "nyc-maurice-conti.jpg",
          guest: ""
        },
        {
          isSpeaker: null,
          firstName: "Stacey",
          lastName: "Cunningham",
          title: "President",
          company: "NYSE",
          headshot: "white-filler.jpg",
          guest: ""
        },
        {
          isSpeaker: true,
          firstName: "Clive",
          lastName: "Davis",
          title: "Chief Creative Officer",
          company: "Sony Music Entertainment",
          headshot: "nyc-clive-davis.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Francisco",
          lastName: "D'Souza",
          title: "Vice Chairman",
          company: "Cognizant",
          headshot: "nyc-francisco-dsouza.jpg",
          guest: "Maria Ines Kavamura"
        },
        {
          isSpeaker: null,
          firstName: "Larry",
          lastName: "De Shon",
          title: "President and CEO",
          company: "Avis Budget Group",
          headshot: "nyc-larry-de_shon.jpg",
          guest: "Gail De Shon"
        },
        {
          isSpeaker: null,
          firstName: "Carmine",
          lastName: "Di Sibio",
          title: "CEO",
          company: "EY",
          headshot: "nyc-carmine-di sibio.jpg",
          guest: "Amy Di Sibio"
        },
        {
          isSpeaker: null,
          firstName: "Brad",
          lastName: "Feldmann",
          title: "Chairman, President and CEO",
          company: "Cubic Corporation",
          headshot: "nyc-bradley-feldmann.jpg",
          guest: "Debora Feldmann"
        },
        {
          isSpeaker: null,
          firstName: "Michael",
          lastName: "Field",
          title: "CEO",
          company: "The Raymond Corporation",
          headshot: "nyc-michael-field.jpg",
          guest: "Robin Tobin"
        },
        {
          isSpeaker: null,
          firstName: "Peter",
          lastName: "Fox",
          title: "Executive Chairman",
          company: "Linfox",
          headshot: "nyc-peter-fox.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Frances",
          lastName: "Frei",
          title: "UPS Foundation Professor of Service Management",
          company: "Harvard Business School",
          headshot: "nyc-frances-frei.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Emerson",
          lastName: "Fullwood",
          title: "",
          company: "",
          headshot: "nyc-emerson-fullwood.jpg",
          guest: "Vernita Fullwood"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Gauba",
          title: "Founder and Managing Director",
          company: "The CXO Fund",
          headshot: "nyc-gary-gauba.jpg",
          guest: "Pooja Gauba"
        },
        {
          isSpeaker: null,
          firstName: "Alexis",
          lastName: "Glick",
          title: "CEO",
          company: "GENYOUth",
          headshot: "nyc-alexis-glick.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Carl",
          lastName: "Grivner",
          title: "CEO",
          company: "Colt Technology Services",
          headshot: "nyc-carl-grivner.jpg",
          guest: "Kaya Phares"
        },
        {
          isSpeaker: true,
          firstName: "Desiree",
          lastName: "Gruber",
          title: "CEO",
          company: "Full Picture",
          headshot: "nyc-desiree-gruber.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Al",
          lastName: "Guido",
          title: "President",
          company: "San Francisco 49ers",
          headshot: "nyc-al-guido.jpg",
          guest: "Thea Guido"
        },
        {
          isSpeaker: null,
          firstName: "C.P.",
          lastName: "Gurnani",
          title: "Managing Director and CEO",
          company: "Tech Mahindra",
          headshot: "nyc-cp-gurnani.JPG"
        },
        {
          isSpeaker: null,
          firstName: "Mike",
          lastName: "Guyette",
          title: "CEO",
          company: "VSP Global",
          headshot: "nyc-michael-guyette.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Paul",
          lastName: "Hermelin",
          title: "Chairman of the Board and CEO",
          company: "Capgemini",
          headshot: "nyc-paul-hermelin.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Andres",
          lastName: "Holzer",
          title: "Member of the Board",
          company: "Dufry-Hudson",
          headshot: "nyc-andres-holzer.jpg",
          guest: "Christine Holzer"
        },

        {
          isSpeaker: null,
          firstName: "Arianna",
          lastName: "Huffington",
          title: "Founder and CEO",
          company: "Thrive Global",
          headshot: "nyc-arianna-huffington.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Colin",
          lastName: "Hunt",
          title: "CEO",
          company: "AIB Ireland",
          headshot: "nyc-colin-hunt.jpg",
          guest: "Nuala Hunt"
        },
        {
          isSpeaker: null,
          firstName: "Dev",
          lastName: "Ittycheria",
          title: "President and CEO ",
          company: "MongoDB",
          headshot: "nyc-dev-ittycheria.jpg",
          guest: "Anju Thomas"
        },
        {
          isSpeaker: null,
          firstName: "Ralph",
          lastName: "Izzo",
          title: "Chairman of the Board, President and CEO",
          company: "PSEG",
          headshot: "nyc-ralph-izzo.jpg",
          guest: " Karen Izzo"
        },
        {
          firstName: "Larry",
          lastName: "Jackson",
          headshot: "nyc-larry-jackson.jpg",
          title: "Global Creative Director",
          company: "Apple Music",
          isSpeaker: true
        },

        {
          isSpeaker: null,
          firstName: "Ken",
          lastName: "Jarin",
          title: "Partner",
          company: "Ballard Spahr LLP",
          headshot: "nyc-ken-jarin.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Derek",
          lastName: "Jeter",
          title: "CEO",
          company: "Miami Marlins",
          headshot: "nyc-derek-jeter.jpg",
          guest: "Hannah Jeter"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Kaplan",
          title: "Chairman and CEO",
          company: "Virginia Mason Medical Center",
          headshot: "nyc-gary-kaplan.jpg",
          guest: "Wendy Kaplan"
        },
        {
          isSpeaker: null,
          firstName: "Shelly",
          lastName: "Kapoor Collins",
          title: "General Partner",
          company: "Shatterfund",
          headshot: "nyc-shelly-kapoor-collins.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Michael",
          lastName: "Kasbar",
          title: "Chairman, President, and CEO",
          company: "World Fuel Services Corporation",
          headshot: "nyc-michael-kasbar.jpg",
          guest: "Mary Kasbar"
        },
        {
          firstName: "Brad",
          lastName: "Katsuyama",
          title: "Co-founder and CEO",
          company: "EX Group",
          headshot: "nyc-brad-katsuyama.jpg",
          guest: "Ashley Katsuyama"
        },
        {
          isSpeaker: null,
          firstName: "Sarah",
          lastName: "Kauss",
          title: "Founder and CEO",
          company: "S'well",
          headshot: "nyc-sarah-kauss.jpg",
          guest: "Jeff Peck"
        },
        {
          isSpeaker: null,
          firstName: "Brad",
          lastName: "Keywell",
          title: "Founder and CEO",
          company: "Uptake",
          headshot: "nyc-brad-keywell.jpg"
        },

        {
          isSpeaker: true,
          firstName: "Christian",
          lastName: "Klein",
          title: "Co-CEO",
          company: "SAP",
          headshot: "nyc-christian-klein.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Wladimir",
          lastName: "Klitschko",
          title: "Founder",
          company: "Klitschko Ventures GmbH",
          headshot: "nyc-wladimir-klitschko.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Howard",
          lastName: "Krein",
          title: "M.D.,PhD, Chief Medical Officer",
          company: "StartUp Health",
          headshot: "nyc-howard-krein.jpg",
          guest: "Bari Krein"
        },

        {
          isSpeaker: null,
          firstName: "Steve",
          lastName: "Krein",
          title: "CEO and Co-founder",
          company: "StartUp Health",
          headshot: "nyc-steven-krein.jpg",
          guest: "Rebecca Krein"
        },
        {
          isSpeaker: null,
          firstName: "Brian",
          lastName: "Krzanich",
          title: "CEO",
          company: "CDK Global",
          headshot: "nyc-brian-krzanich.jpg",
          guest: "Brandee Krzanich"
        },
        {
          isSpeaker: null,
          firstName: "Ken",
          lastName: "Lamneck",
          title: "President and CEO",
          company: "Insight",
          headshot: "nyc-ken-lamneck.jpg",
          guest: "Marianne Lamneck"
        },

        {
          firstName: "Stephanie",
          lastName: "Lampkin",
          title: "Founder and CEO",
          company: "Blendoor",
          headshot: "nyc-stephanie-lampkin.jpg",
          isSpeaker: true
        },
        {
          isSpeaker: null,
          firstName: "Kim",
          lastName: "Le",
          title: "CEO",
          company: "A2Q2",
          headshot: "nyc-kim-le.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Aaron",
          lastName: "Levie",
          title: "CEO, Co-founder and Chairman",
          company: "Box, Inc.",
          headshot: "nyc-aaron-levie.jpg"
        },
        {
          isSpeaker: null,
          firstName: "William",
          lastName: "Lewis",
          title: "CEO, Dow Jones & Company and Publisher",
          company: "The Wall Street Journal",
          headshot: "nyc-william-lewis.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Chad",
          lastName: "Linebaugh",
          title: "President",
          company: "Sundance",
          headshot: "nyc-chad-linebaugh.jpg",
          guest: "Amber Linebaugh"
        },
        {
          isSpeaker: null,
          firstName: "James",
          lastName: "Loree",
          title: "President and CEO",
          company: "Stanley Black & Decker",
          headshot: "nyc-james-loree.jpg",
          guest: "Rebecca Loree"
        },
        {
          isSpeaker: null,
          firstName: "Peter",
          lastName: "Martyr",
          title: "Global Chief Executive",
          company: "Norton Rose Fulbright",
          headshot: "nyc-peter-martyr.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Bill",
          lastName: "McDermott",
          title: "CEO Emeritus and Member of the Executive Board",
          company: "SAP",
          headshot: "mcdermott.jpg",
          guest: "Julie McDermott"
        },
        {
          isSpeaker: null,
          firstName: "Sanjay",
          lastName: "Mehrotra",
          title: "President and CEO",
          company: "Micron Technology, Inc.",
          headshot: "nyc-sanjay-mehrotra.jpg",
          guest: "Sangeeta Mehrotra"
        },
        {
          isSpeaker: true,
          firstName: "Hasan",
          lastName: "Minhaj",
          title: "Host and Creator",
          company: "Patriot Act with Hasan Minhaj",
          headshot: "nyc-hasan-minhaj.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Rebecca",
          lastName: "Minkoff",
          title: "Co-founder and Creative Director",
          company: "Rebecca Minkoff",
          headshot: "nyc-rebecca-minkoff.jpg",
          guest: "Gavin Bellour"
        },
        {
          isSpeaker: null,
          firstName: "Ken",
          lastName: "Moelis",
          title: "Chairman and CEO",
          company: "Moelis & Company",
          headshot: "nyc-ken-moelis.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Carlos Creus",
          lastName: "Moreira",
          title: "Founder, Chairman, and CEO",
          company: "WISeKey",
          headshot: "nyc-carlos-moreira.jpg",
          guest: "Anne Feuardent Creus Moreira"
        },
        {
          isSpeaker: true,
          firstName: "Jennifer",
          lastName: "Morgan",
          title: "Co-CEO",
          company: "SAP",
          headshot: "nyc-jennifer-morgan.jpg",
          guest: "Michael Morgan"
        },
        {
          isSpeaker: true,
          firstName: "Anne",
          lastName: "Morris",
          title: "Executive Founder",
          company: "The Leadership Consortium",
          headshot: "nyc-anne-morriss.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Matt",
          lastName: "Murray",
          title: "Editor-in-Chief",
          company: "WSJ",
          headshot: "nyc-matt-murray.jpg",
          guest: "Janine Flory"
        },
        {
          isSpeaker: null,
          firstName: "Sudheesh",
          lastName: "Nair",
          title: "CEO",
          company: "ThoughtSpot",
          headshot: "nyc-sudheesh-nair.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Tom",
          lastName: "Nelson",
          title: "Chairman, President, and CEO",
          company: "National Gypsum Company",
          headshot: "nyc-thomas-nelson.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Antonio",
          lastName: "Neri",
          title: "President and CEO",
          company: "Hewlett Packard Enterprise",
          headshot: "nyc-antonio-neri.jpg"
        },
        {
          isSpeaker: null,
          firstName: "X. Rick",
          lastName: "Niu",
          title: "President and CEO",
          company: "Starr Strategic Holdings",
          headshot: "nyc-rick-niu.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Todd",
          lastName: "Olson",
          title: "Founder and CEO",
          company: "Pendo",
          headshot: "nyc-todd-olson.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Scott",
          lastName: "O'Neil",
          title: "CEO",
          company: "Harris Blitzer Sports & Entertainment",
          headshot: "nyc-scott-oneil.jpg",
          guest: "Lisa O’Neil"
        },
        {
          isSpeaker: null,
          firstName: "Eugenio",
          lastName: "Pace",
          title: "CEO",
          company: "Auth0",
          headshot: "nyc-eugenio-pace.jpg",
          guest: "Magdalena Bargero"
        },
        {
          isSpeaker: null,
          firstName: "Joe",
          lastName: "Papa",
          title: "Chairman and CEO",
          company: "Bausch Health Companies",
          headshot: "nyc-joseph-papa.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Chemi",
          lastName: "Peres",
          title: "Managing General Partner and Co-Founder",
          company: "Pitango Venture Capital",
          headshot: "nyc-chemi-peres.jpg",
          guest: "Gila Peres"
        },
        {
          isSpeaker: null,
          firstName: "Kevin",
          lastName: "Plank",
          title: "Chairman and CEO",
          company: "Under Armour",
          headshot: "nyc-kevin-plank.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Edmund",
          lastName: "Pribitkin",
          title: "Chief Medical Officer",
          company: "Thomas Jefferson University",
          headshot: "nyc-edmund-pribitkin.jpg",
          guest: ""
        },
        {
          isSpeaker: null,
          firstName: "Frank",
          lastName: "Quattrone",
          title: "Founder and Executive Chairman",
          company: "Qatalyst Partners",
          headshot: "nyc-frank-quattrone.jpg",
          guest: "Denise Foderaro"
        },

        {
          isSpeaker: null,
          firstName: "Marc",
          lastName: "Raibert",
          title: "Founder and CEO",
          company: "Boston Dynamics",
          headshot: "nyc-marc-raibert.jpg",
          guest: "Nancy Cornelius"
        },
        {
          isSpeaker: null,
          firstName: "Punit",
          lastName: "Renjen",
          title: "Global CEO",
          company: "Deloitte Global",
          headshot: "nyc-punit-renjen.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Liz",
          lastName: "Robbins",
          title: "Owner and CEO",
          company: "Liz Robbins Associates",
          headshot: "nyc-liz-robbins.jpg",
          guest: ""
        },

        {
          isSpeaker: null,
          firstName: "John",
          lastName: "Rosanvallon",
          title: "President and CEO",
          company: "Dassault Falcon Jet",
          headshot: "nyc-john-rosanvallon.jpg",
          guest: "Genevieve Rosanvallon"
        },
        {
          isSpeaker: null,
          firstName: "Howie",
          lastName: "Roseman",
          title: "Executive Vice President of Football Operations",
          company: "Philadelphia Eagles",
          headshot: "nyc-howie-roseman.jpg",
          guest: "Mindy Roseman"
        },
        {
          isSpeaker: null,
          firstName: "Stephen",
          lastName: "Ross",
          title: "Chairman",
          company: "Related Companies",
          headshot: "nyc-stephen-ross.jpg",
          guest: "Kara Ross"
        },
        {
          isSpeaker: null,
          firstName: "Anthoni",
          lastName: "Salim",
          title: "President and CEO",
          company: "Salim Group",
          headshot: "nyc-anthoni-salim.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Ramin",
          lastName: "Sayar",
          title: "President and CEO",
          company: "Sumo Logic",
          headshot: "nyc-ramin-sayar.jpg",
          guest: "Samantha Sayar"
        },
        {
          isSpeaker: null,
          firstName: "Geoff",
          lastName: "Scott",
          title: "CEO",
          company: "ASUG",
          headshot: "nyc-geoff-scott.jpg",
          guest: "Jennifer Scott"
        },

        {
          isSpeaker: null,
          firstName: "Robert",
          lastName: "Siegfried Jr.",
          title: "Founder and CEO",
          company: "The Siegfried Group, LLP",
          headshot: "nyc-rob-siegfried.jpg",
          guest: "Kathy Siegfried"
        },
        {
          isSpeaker: null,
          firstName: "Adam",
          lastName: "Silver",
          title: "Commissioner",
          company: "NBA",
          headshot: "nyc-adam-silver.jpg"
        },

        {
          isSpeaker: null,
          firstName: "Randy",
          lastName: "Skoda",
          title: "President and CEO",
          company: "Topco Associates, LLC",
          headshot: "nyc-randy-skoda.jpg",
          guest: "Julie Skoda"
        },
        {
          isSpeaker: null,
          firstName: "Ryan",
          lastName: "Smith",
          title: "CEO",
          company: "Qualtrics",
          headshot: "nyc-ryan-smith.jpg",
          guest: "Ashley Smith"
        },
        {
          isSpeaker: null,
          firstName: "John",
          lastName: "Snyder",
          title: "President and CEO",
          company: "BCD Travel",
          headshot: "nyc-john-snyder.jpg",
          guest: "DeeAnna Lynn Cole"
        },
        {
          isSpeaker: null,
          firstName: "Jim",
          lastName: "Spradlin",
          title: "CEO",
          company: "GROWMARK, Inc.",
          headshot: "nyc-jim-spradlin.jpg",
          guest: "Lisbeth Spradlin"
        },
        {
          isSpeaker: null,
          firstName: "Dan",
          lastName: "Springer",
          title: "CEO",
          company: "DocuSign",
          headshot: "nyc-dan-springer.jpg",
          guest: "Lisa Coscino"
        },
        {
          isSpeaker: true,
          firstName: "Julie",
          lastName: "Sweet",
          title: "CEO",
          company: "Accenture",
          headshot: "nyc-julie-sweet.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Mike",
          lastName: "Tannenbaum",
          title: "Front Office Insider",
          company: "ESPN",
          headshot: "nyc-mike-tannenbaum.jpg",
          guest: "Michelle Tannenbaum"
        },

        {
          isSpeaker: null,
          firstName: "Jennifer",
          lastName: "Tejada",
          title: "CEO",
          company: "PagerDuty",
          headshot: "nyc-jennifer-tejada.jpg"
        },

        {
          isSpeaker: null,
          firstName: "Johannes",
          lastName: "Teyssen",
          title: "Chairman and CEO",
          company: "E.ON",
          headshot: "nyc-johannes-teyssen.jpg",
          guest: "Doris Pingel-Teyssen"
        },
        {
          isSpeaker: null,
          firstName: "Therese",
          lastName: "Tucker",
          title: "Founder and CEO",
          company: "BlackLine",
          headshot: "nyc-therese-tucker.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Glen",
          lastName: "Tullman",
          title: "Chairman",
          company: "Livongo Health",
          headshot: "nyc-glen-tullman.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Nick",
          lastName: "Tzitzon",
          title: "EVP, Marketing and Communications",
          company: "SAP",
          headshot: "nyc-nick-tzitzon.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Andreas",
          lastName: "Umbach",
          title: "Chairman",
          company: "Landis+Gyr AG",
          headshot: "nyc-andreas-umbach.jpg",
          guest: "Karen Umbach"
        },
        {
          isSpeaker: null,
          firstName: "Vijay",
          lastName: "Kumar C",
          title: "President and CEO",
          company: "HCL Technologies",
          headshot: "nyc-c-vijayakumar.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Nick",
          lastName: "Vlahos",
          title: "CEO",
          company: "The Honest Company",
          headshot: "nyc-nick-vlahos.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Lindsey",
          lastName: "Vonn",
          title: "Olympic Alpine Skier",
          company: "",
          headshot: "nyc-lindsey-vonn.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Mark",
          lastName: "Weinberger",
          title: "Former Global Chairman and CEO",
          company: "EY",
          headshot: "nyc-mark-weinberger.jpg",
          guest: "Nancy Weinberger"
        },
        {
          isSpeaker: null,
          firstName: "Maggie",
          lastName: "Wilderotter",
          title: "Chairman and CEO",
          company: "Grand Reserve Inn",
          headshot: "nyc-maggie-wilderotter.jpg",
          guest: "Jay Wilderotter"
        },
        {
          isSpeaker: null,
          firstName: "John",
          lastName: "Wren",
          title: "Chairman and CEO",
          company: "Omnicom Group",
          headshot: "nyc-john-wren.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Hal",
          lastName: "Yoh",
          title: "Chairman and CEO",
          company: "Day & Zimmermann",
          headshot: "nyc-hal-yoh.jpg",
          guest: "Sharon Yoh"
        },
        {
          isSpeaker: null,
          firstName: "Jed",
          lastName: "York",
          title: "CEO",
          company: "San Francisco 49ers",
          headshot: "nyc-jed-york.jpg",
          guest: ""
        },
        {
          isSpeaker: true,
          firstName: "Strauss",
          lastName: "Zelnick ",
          title: "Chairman and CEO",
          company: "Take-Two Interactive",
          headshot: "nyc-strauss-zelnick.jpg"
        }
      ]
    };
  },
  computed: {
    filteredList: function() {
      let parts_arr = this.participants.slice(),
        searchString = this.search;
      if (!searchString) {
        return parts_arr;
      }

      searchString = searchString.trim().toLowerCase();

      parts_arr = parts_arr.filter(function(part) {
        if (part.firstName.toLowerCase().indexOf(searchString) !== -1) {
          return part;
        } else if (part.lastName.toLowerCase().indexOf(searchString) !== -1) {
          return part;
        } else if (part.title.toLowerCase().indexOf(searchString) !== -1) {
          return part;
        } else if (
          (
            part.firstName.toLowerCase() +
            " " +
            part.lastName.toLowerCase()
          ).indexOf(searchString) !== -1
        ) {
          return part;
        } else if (part.company.toLowerCase().indexOf(searchString) !== -1) {
          return part;
        }
      });

      return parts_arr;
    }
  }
});
