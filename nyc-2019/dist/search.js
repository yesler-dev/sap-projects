const app = new Vue({
  el: "#app",
  delimiters: ["${", "}"],
  data: function() {
    return {
      search: "",
      participants: [
        {
          isSpeaker: null,
          firstName: "Yousef Abdullah",
          lastName: "Al-Benyan",
          title: "Vice Chairman and CEO",
          company: "Saudi Basic Industries",
          headshot: "nyc-al-benyan.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Mutlaq",
          lastName: "Almorished",
          title: "CEO",
          company: "Tasnee",
          headshot: "nyc-mutlaq-almorished.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Bettman",
          title: "Commissioner",
          company: "NHL",
          headshot: "nyc-gary-bettman.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Marco",
          lastName: "Bizzarri",
          title: "CEO",
          company: "Gucci",
          headshot: "nyc-marco-bizzarri.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Dominic",
          lastName: "Blakemore",
          title: "Group CEO",
          company: "Compass Group",
          headshot: "nyc-dominic-blakemore.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Aryeh B.",
          lastName: "Bourkoff",
          title: "Founder and CEO",
          company: "LionTree LLC",
          headshot: "nyc-aryeh-bourkoff.jpeg"
        },
        {
          isSpeaker: null,
          firstName: "Dave",
          lastName: "Brandon",
          title: "Chairman",
          company: "Domino's Pizza, Inc.",
          headshot: "nyc-dave-brandon.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Cohn",
          title:
            "Business leader, philanthropist and the former Director of the United States National Economic Council",
          company: "",
          headshot: "nyc-gary-cohn.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Alan",
          lastName: "Colberg",
          title: "CEO",
          company: "Assurant, Inc.",
          headshot: "nyc-alan-colberg.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Marlene",
          lastName: "Colucci-Renna",
          title: "Executive Director",
          company: "The Business Council",
          headshot: "nyc-marlene-colucci-renna.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Clive",
          lastName: "Davis",
          title: "Chief Creative Officer",
          company: "Sony Music Entertainment",
          headshot: "nyc-clive-davis.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Francisco",
          lastName: "D'Souza",
          title: "Vice Chairman and CEO",
          company: "Cognizant",
          headshot: "nyc-francisco-dsouza.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Larry",
          lastName: "De Shon",
          title: "President and CEO",
          company: "Avis Budget Group",
          headshot: "nyc-larry-de_shon.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Carmine",
          lastName: "Di Sibio",
          title: "CEO",
          company: "EY",
          headshot: "nyc-carmine-di sibio.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Brad",
          lastName: "Feldmann",
          title: "President and CEO",
          company: "Cubic Corporation",
          headshot: "nyc-bradley-feldmann.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Tony",
          lastName: "Fernandes",
          title: "Co-founder and CEO",
          company: "AirAsia",
          headshot: "white-filler.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Michael",
          lastName: "Field",
          title: "CEO",
          company: "The Raymond Corporation",
          headshot: "nyc-michael-field.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Peter",
          lastName: "Fox",
          title: "Executive Chairman",
          company: "Linfox",
          headshot: "nyc-peter-fox.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Gauba",
          title: "Founder and Managing Director",
          company: "The CXO Fund",
          headshot: "nyc-gary-gauba.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Alexis",
          lastName: "Glick",
          title: "CEO",
          company: "GENYOUth Foundation",
          headshot: "nyc-alexis-glick.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Carl",
          lastName: "Grivner",
          title: "CEO",
          company: "Colt Technology Services",
          headshot: "nyc-carl-grivner.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Desiree",
          lastName: "Gruber",
          title: "CEO",
          company: "Full Picture",
          headshot: "nyc-desiree-gruber.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Al",
          lastName: "Guido",
          title: "President",
          company: "San Francisco 49ers",
          headshot: "nyc-al-guido.jpg"
        },
        {
          isSpeaker: null,
          firstName: "C.P.",
          lastName: "Gurnani",
          title: "Managing Director and CEO",
          company: "Tech Mahindra",
          headshot: "nyc-cp-gurnani.JPG"
        },
        {
          isSpeaker: null,
          firstName: "Michael",
          lastName: "Guyette",
          title: "CEO",
          company: "VSP Global",
          headshot: "nyc-michael-guyette.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Paul",
          lastName: "Hermelin",
          title: "Chairman of the Board and CEO",
          company: "Capgemini",
          headshot: "nyc-paul-hermelin.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Arianna",
          lastName: "Huffington",
          title: "Founder and CEO",
          company: "Thrive Global",
          headshot: "nyc-arianna-huffington.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Rich",
          lastName: "Hume",
          title: "CEO",
          company: "Tech Data Corporation",
          headshot: "nyc-rich-hume.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Colin",
          lastName: "Hunt",
          title: "CEO",
          company: "AIB Ireland",
          headshot: "nyc-colin-hunt.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Dev",
          lastName: "Ittycheria",
          title: "President and CEO ",
          company: "MongoDB",
          headshot: "nyc-dev-ittycheria.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Ralph",
          lastName: "Izzo",
          title: "Chairman of the Board, President and CEO",
          company: "PSEG",
          headshot: "white-filler.jpg"
        },
        {
          firstName: "Larry",
          lastName: "Jackson",
          headshot: "nyc-larry-jackson.jpg",
          title: "Global Creative Director",
          company: "Apple Music",
          isSpeaker: true
        },
        {
          isSpeaker: null,
          firstName: "Jo Ann",
          lastName: "Jenkins",
          title: "CEO",
          company: "AARP",
          headshot: "nyc-joann-jenkins.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Kaplan",
          title: "Chairman and CEO",
          company: "Virginia Mason Medical Center",
          headshot: "nyc-gary-kaplan.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Michael",
          lastName: "Kasbar",
          title: "Chairman, President, and CEO",
          company: "World Fuel Services Corporation",
          headshot: "nyc-michael-kasbar.jpg"
        },
        {
          firstName: "Brad",
          lastName: "Katsuyama",
          title: "Co-founder and CEO",
          company: "EX Group",
          headshot: "nyc-brad-katsuyama.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Sarah",
          lastName: "Kauss",
          title: "Founder and CEO",
          company: "S'well",
          headshot: "nyc-sarah-kauss.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Wladimir",
          lastName: "Klitschko",
          title: "Founder",
          company: "Klitschko Ventures GmbH",
          headshot: "white-filler.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Brian",
          lastName: "Krzanich",
          title: "CEO",
          company: "CDK Global",
          headshot: "nyc-brian-krzanich.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Ken",
          lastName: "Lamneck",
          title: "President and CEO",
          company: "Insight",
          headshot: "nyc-ken-lamneck.jpg"
        },
        {
          firstName: "Stephanie",
          lastName: "Lampkin",
          title: "Founder and CEO",
          company: "Blendoor",
          headshot: "nyc-stephanie-lampkin.jpg",
          isSpeaker: true
        },
        {
          isSpeaker: null,
          firstName: "Aaron",
          lastName: "Levie",
          title: "CEO, Co-founder and Chairman",
          company: "Box, Inc.",
          headshot: "nyc-aaron-levie.jpg"
        },
        {
          isSpeaker: null,
          firstName: "William",
          lastName: "Lewis",
          title: "CEO and Publisher, The Wall Street Journal",
          company: "Dow Jones",
          headshot: "nyc-william-lewis.jpg"
        },
        {
          isSpeaker: null,
          firstName: "James",
          lastName: "Loree",
          title: "President and CEO",
          company: "Stanley Black & Decker",
          headshot: "nyc-james-loree.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Peter",
          lastName: "Martyr",
          title: "Global Chief Executive",
          company: "Norton Rose Fulbright",
          headshot: "nyc-peter-martyr.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Manuel",
          lastName: "Medina",
          title: "CEO",
          company: "Cyxtera",
          headshot: "nyc-manuel-medina.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Sanjay",
          lastName: "Mehrotra",
          title: "President and CEO",
          company: "Micron Technology, Inc.",
          headshot: "nyc-sanjay-mehrotra.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Hasan",
          lastName: "Minhaj",
          title: "Host and Creator",
          company: "Patriot Act with Hasan Minhaj",
          headshot: "nyc-hasan-minhaj.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Rebecca",
          lastName: "Minkoff",
          title: "Co-founder and Creative Director",
          company: "Rebecca Minkoff",
          headshot: "nyc-rebecca-minkoff.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Ken",
          lastName: "Moelis",
          title: "Chairman and CEO",
          company: "Moelis & Company",
          headshot: "nyc-ken-moelis.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Carlos Creus",
          lastName: "Moreira",
          title: "Founder, Chairman, and CEO",
          company: "WISeKey",
          headshot: "nyc-carlos-moreira.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Robert",
          lastName: "Mortiz",
          title: "Global Chairman",
          company: "PwC",
          headshot: "nyc-robert-moritz.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Ravi Chandra",
          lastName: "Muddada",
          title: "Principal Finance Secretary",
          company: "Government of Andhra Pradesh",
          headshot: "nyc-ravi-chandra.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Shantanu",
          lastName: "Narayen",
          title: "Chairman and CEO",
          company: "Adobe",
          headshot: "nyc-shantanu-narayen.JPG"
        },
        {
          isSpeaker: null,
          firstName: "Thomas C.",
          lastName: "Nelson",
          title: "Chairman, President, and CEO",
          company: "National Gypsum Company",
          headshot: "nyc-thomas-nelson.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Antonio",
          lastName: "Neri",
          title: "President and CEO",
          company: "Hewlett Packard Enterprise",
          headshot: "nyc-antonio-neri.jpg"
        },
        {
          isSpeaker: null,
          firstName: "X. Rick",
          lastName: "Niu",
          title: "President and CEO",
          company: "Starr Strategic Holdings",
          headshot: "nyc-rick-niu.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Scott",
          lastName: "O'Neil",
          title: "CEO",
          company: "Harris Blitzer Sports & Entertainment",
          headshot: "nyc-scott-oneil.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Joseph",
          lastName: "Papa",
          title: "Chairman and CEO",
          company: "Bausch Health Companies",
          headshot: "nyc-joseph-papa.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Chemi",
          lastName: "Peres",
          title: "Managing General Partner and Co-Founder",
          company: "Pitango Venture Capital",
          headshot: "nyc-chemi-peres.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Stefano",
          lastName: "Pessina",
          title: "Executive Vice Chairman and CEO",
          company: "Walgreens Boots Alliance",
          headshot: "nyc-stefano-pessina.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Kevin",
          lastName: "Plank",
          title: "Chairman and CEO",
          company: "Under Armour",
          headshot: "nyc-kevin-plank.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Gary",
          lastName: "Pruitt",
          title: "President and CEO",
          company: "The Associated Press",
          headshot: "nyc-gary-pruitt.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Frank",
          lastName: "Quattrone",
          title: "Executive Chairman",
          company: "Qatalyst Partners",
          headshot: "nyc-frank-quattrone.jpg"
        },

        {
          isSpeaker: null,
          firstName: "Marc",
          lastName: "Raibert",
          title: "CEO",
          company: "Boston Dynamics",
          headshot: "nyc-marc-raibert.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Punit",
          lastName: "Renjen",
          title: "CEO",
          company: "Deloitte Global",
          headshot: "nyc-punit-renjen.jpg"
        },
        {
          isSpeaker: null,
          firstName: "John",
          lastName: "Rosanvallon",
          title: "President and CEO",
          company: "Dassault Falcon Jet",
          headshot: "nyc-john-rosanvallon.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Stephen",
          lastName: "Ross",
          title: "Chairman",
          company: "Related Companies",
          headshot: "nyc-stephen-ross.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Panu",
          lastName: "Routila",
          title: "President and CEO",
          company: "Konecranes Plc",
          headshot: "nyc-panu-routila.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Anthoni",
          lastName: "Salim",
          title: "President and CEO",
          company: "Salim Group",
          headshot: "nyc-anthoni-salim.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Ramin",
          lastName: "Sayar",
          title: "President and CEO",
          company: "Sumo Logic",
          headshot: "nyc-ramin-sayar.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Dan",
          lastName: "Schulman",
          title: "President and CEO",
          company: "PayPal",
          headshot: "nyc-dan-schulman.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Geoff",
          lastName: "Scott",
          title: "CEO",
          company: "ASUG",
          headshot: "nyc-geoff-scott.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Adam",
          lastName: "Silver",
          title: "Commissioner",
          company: "NBA",
          headshot: "nyc-adam-silver.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Randy",
          lastName: "Skoda",
          title: "President and CEO",
          company: "Topco Associates, LLC",
          headshot: "nyc-randy-skoda.jpg"
        },
        {
          isSpeaker: null,
          firstName: "John",
          lastName: "Snyder",
          title: "President and CEO",
          company: "BCD Travel",
          headshot: "nyc-john-snyder.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Dan",
          lastName: "Springer",
          title: "CEO",
          company: "DocuSign",
          headshot: "nyc-dan-springer.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Jim",
          lastName: "Spradlin",
          title: "CEO",
          company: "GROWMARK, Inc.",
          headshot: "nyc-jim-spradlin.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Jennifer",
          lastName: "Tejada",
          title: "CEO",
          company: "PagerDuty",
          headshot: "nyc-jennifer-tejada.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Johannes",
          lastName: "Teyssen",
          title: "Chairman and CEO",
          company: "E.ON",
          headshot: "nyc-johannes-teyssen.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Ashish J.",
          lastName: "Thakkar",
          title: "Founder and Chairman",
          company: "Mara Group",
          headshot: "nyc-ashish-thakkar.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Glen",
          lastName: "Tullman",
          title: "CEO",
          company: "Livongo Health",
          headshot: "nyc-glen-tullman.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Andreas",
          lastName: "Umbach",
          title: "Chairman",
          company: "Landis+Gyr AG",
          headshot: "nyc-andreas-umbach.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Jacques",
          lastName: "van den Broek",
          title: "CEO and Chairman of the Executive Board",
          company: "Randstad Holding NV",
          headshot: "nyc-jacques-van-den-broek.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Krishnadevarayalu",
          lastName: "Vasireddy",
          title: "CEO",
          company: "APCFSS, Government of Andhra Pradesh",
          headshot: "nyc-krishna-vasireddy.jpg"
        },
        {
          isSpeaker: null,
          firstName: "C.",
          lastName: "Vijayakumar",
          title: "President and CEO",
          company: "HCL Technologies",
          headshot: "nyc-c-vijayakumar.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Nick",
          lastName: "Vlahos",
          title: "CEO",
          company: "The Honest Company",
          headshot: "nyc-nick-vlahos.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Lindsey",
          lastName: "Vonn",
          title: "Olympic Alpine Skier",
          company: "",
          headshot: "nyc-lindsey-vonn.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Mark",
          lastName: "Weinberger",
          title: "Former Global Chairman and CEO",
          company: "EY",
          headshot: "nyc-mark-weinberger.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Maggie",
          lastName: "Wilderotter",
          title: "Chairman and CEO",
          company: "Grand Reserve Inn",
          headshot: "nyc-maggie-wilderotter.jpg"
        },
        {
          isSpeaker: null,
          firstName: "John",
          lastName: "Wren",
          title: "President and CEO",
          company: "Omnicom Group",
          headshot: "nyc-john-wren.jpg"
        },
        {
          isSpeaker: null,
          firstName: "Hal",
          lastName: "Yoh",
          title: "Chairman and CEO",
          company: "Day & Zimmermann",
          headshot: "nyc-hal-yoh.jpg"
        },
        {
          isSpeaker: true,
          firstName: "Strauss",
          lastName: "Zelnick ",
          title: "Chairman and CEO",
          company: "Take-Two Interactive",
          headshot: "nyc-strauss-zelnick.jpg"
        }
      ]
    };
  },
  computed: {
    filteredList: function() {
      let parts_arr = this.participants,
        searchString = this.search;
      if (!searchString) {
        return parts_arr;
      }

      searchString = searchString.trim().toLowerCase();

      parts_arr = parts_arr.filter(function(part) {
        if (part.firstName.toLowerCase().indexOf(searchString) !== -1) {
          return part;
        } else if (part.lastName.toLowerCase().indexOf(searchString) !== -1) {
          return part;
        } else if (part.title.toLowerCase().indexOf(searchString) !== -1) {
          return part;
        } else if (part.company.toLowerCase().indexOf(searchString) !== -1) {
          return part;
        }
      });

      return parts_arr;
    }
  }
});
