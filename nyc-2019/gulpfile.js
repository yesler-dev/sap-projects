// Include gulp
var gulp = require('gulp');

// Include plugins
var fs             = require('fs');
var sass           = require('gulp-sass');
var concat         = require('gulp-concat');
var uglify         = require('gulp-uglify');
var livereload     = require('gulp-livereload');
var connect        = require('gulp-connect');
var plumber        = require('gulp-plumber');
var postcss        = require('gulp-postcss');
var autoprefixer   = require('autoprefixer');
var rename         = require('gulp-rename');
var replace        = require('gulp-replace');
var cssmin         = require('gulp-cssmin');
var concatCss      = require('gulp-concat-css');
var nunjucksRender = require('gulp-nunjucks-render');
var gulpif         = require('gulp-if');
var data           = require('gulp-data');

// Live Reload
gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});

// JS files
gulp.task('js', function() {
    gulp.src('js/**/*.js')
        .pipe(livereload())
        .pipe(gulp.dest('./dist'));
});

// json files
gulp.task('json', function() {
    gulp.src('js/**/*.json')
        .pipe(livereload());
});

// HTML files
gulp.task('html', function() {
    gulp.src('*.html')
        .pipe(livereload());
});

// Compile Sass
gulp.task('sass', function() {
    return gulp.src('src/sass/*.scss')
        .pipe(sass())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest('./'))
        .pipe(livereload());
});

// minify styles
gulp.task('min', function() {
    return gulp.src(['./style.css', './template.html'])
        .pipe(cssmin())
        .pipe(gulp.dest('./dist'));
});

// Compile nunjucks files
gulp.task('nunjucks', function() {
    // Gets .nunjucks files in pages
    return gulp.src('src/pages/*.nunjucks')
    // Add data to Nunjucks
    .pipe(data(function(file) {
        // return require('./js/agenda.json')
        return JSON.parse(fs.readFileSync('./js/agenda.json'));
    }))
    // Add data to Nunjucks
    .pipe(data(function(file) {
        // return require('./js/agenda.json')
        return JSON.parse(fs.readFileSync('./js/speakers.json'));
    }))
    // Add data to Nunjucks
    .pipe(data(function(file) {
        // return require('./js/agenda.json')
        return JSON.parse(fs.readFileSync('./js/participants.json'));
    }))
    // Renders template with nunjucks
    .pipe(nunjucksRender({
        path: ['src/templates/']
    }))
    // output files in root folder
    .pipe(gulp.dest('./'))
    .pipe(livereload());
});

gulp.task('mktonunjucks', function() {
    return gulp.src('src/mktoPages/*.nunjucks')
        .pipe(nunjucksRender({
            path: ['src/templates/']
        }))
        .pipe(gulp.dest('./templates/'))
        .pipe(livereload());
});

// URL replace for Marketo

var mktoPath = 'http://resources.synnexcorp.com/rs/707-ZFS-030/images';

gulp.task('mktoreplace', function() {
    return gulp.src(['./style.css', './templates/*.html'])
        .pipe(replace('./img/logos', mktoPath))
        .pipe(replace('./img/icons', mktoPath))
        .pipe(replace('./img/design', mktoPath))
        .pipe(replace('url("./img/backgrounds', 'url("http://resources.synnexcorp.com/rs/707-ZFS-030/images'))
        .pipe(replace('rel=stylesheet href="./', 'rel=stylesheet href="http://resources.synnexcorp.com/rs/707-ZFS-030/images/'))
        .pipe(cssmin())
        .pipe(gulp.dest('./dist/mkto'));
});

gulp.task('mktorename', function() {
    gulp.src(["./dist/mkto/**/*.css"])
    .pipe(gulpif('style.css', rename('ceo-summit-style.css')))
    .pipe(gulp.dest('./dist/mkto'));
});
// Watch files for changes


var filesToWatch = [
    'src/sass/**/*.scss',
    'src/pages/*.nunjucks',
    'src/templates/**/*.nunjucks',
    'js/**/*.js',
    'js/**/*.json'
]

gulp.task('watch', function() {
    livereload.listen();
    // gulp.watch(filesToWatch, ['nunjucks', 'sass', 'js'])
    gulp.watch('src/sass/**/*.scss', ['sass']);
    gulp.watch('src/pages/*.nunjucks', ['nunjucks']);
    gulp.watch('src/mktoPages/*.nunjucks', ['mktonunjucks']);
    gulp.watch('js/**/*.js', ['js']);
    gulp.watch('js/**/*.json', ['json', 'nunjucks']);
    gulp.watch('src/templates/**/*.nunjucks', ['nunjucks', 'mktonunjucks']);
});


// Default Task
gulp.task('default', ['nunjucks', 'mktonunjucks', 'sass', 'json', 'watch', 'connect']);
gulp.task('mkto', ['mktoreplace', 'mktorename']);


