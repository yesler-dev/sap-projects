$(document).ready(function() {
  var $img = $('#img-mirror img');
  var $imgDiv = $('#img-mirror');
  var containerWidth = $('.black-bg .container').outerWidth();
  var colwidth = $('.left-column').width()

  $imgDiv.css('height', $img.height())
  $img.css('width', ( $(window).width() - containerWidth / 2) )

  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
})

$(window).on('load resize', function() {
  var $img = $('#img-mirror img');
  var $imgDiv = $('#img-mirror');
  var containerWidth = $('.black-bg .container').outerWidth();
  var colwidth = $('.left-column').width()

  $imgDiv.css('height', $img.height())
  $img.css('width', ( $(window).width() - containerWidth / 2) )

 })
