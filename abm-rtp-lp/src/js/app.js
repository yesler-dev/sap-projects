const forms = require('./forms')
import {MDCTabBar} from '@material/tab-bar';
import {MDCDialog} from '@material/dialog';

const mdc = require('material-components-web')
mdc.autoInit()

// init the tab bar
const tabBar = new MDCTabBar(document.querySelector('.mdc-tab-bar'));

// dialog
const dialog = new MDCDialog(document.querySelector('#my-mdc-dialog'))

document.querySelector('#activate-dialog').addEventListener('click', function(e) {
  console.log('hello')
  dialog.show()
})