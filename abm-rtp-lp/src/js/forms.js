const mdc = require('material-components-web')

// TEXT INPUTS
function mdcTextInputizer(input) {
  // make sure we don't run this when it's already been run
  if ( ! $(input).hasClass('mdc-text-field__input') ) {
    // stash important elements
    const $inputWrap = $(input).parent()
    const $label = $('label', $inputWrap).addClass('mdc-floating-label')

    /* SHOW ASTERISKS if you really want to

      const $asterisk = $('.mktoAsterix', $inputWrap)

      if ($asterisk) {
        $('.mktoAsterix', $inputWrap).remove()
        $label.append('<sup>*</sup>')
      }

    **** OR *****
    */

    // REMOVE ASTERSISKS
    $('.mktoAsterix', $inputWrap).remove()

    // delete the label so we can reposition
    $('label', $inputWrap).remove()

    // fix the date label above because date is a really inconsistent input
    if ( $(input).hasClass('mktoDateField') ) {
      $label.addClass('hovered-label') // pairs with the stylesheet
    }

    // add mdc classes, attrs, append html
    $(input)
      .attr('placeholder', '') // clear the placeholders since our labels do that work
      .addClass('mdc-text-field__input') // add the appropriate class for MDC

    $inputWrap
      .addClass('mdc-text-field working-now') // MDC classes
      .attr('data-mdc-auto-init', 'MDCTextField') // auto init that MDC magic
      .append($label) // attach the label where it should go
      .append('<div class="mdc-line-ripple"></div>') // line ripple? of course you want it

  }
}

// TEXTAREAS
function mdcTextAreaizer(input) {
  // See mdcTextInputizer() for details
  if ( ! $(input).hasClass('mdc-text-field__input') ) {
    const $inputWrap = $(input).parent()
    const $label = $('label', $inputWrap).addClass('mdc-floating-label')

    $('.mktoAsterix', $inputWrap).remove()

    $('label', $inputWrap).remove()

    $(input)
      .attr('placeholder', '')
      .addClass('mdc-text-field__input')

    $inputWrap
      .addClass('mdc-text-field text-field mdc-text-field--textarea')
      .attr('data-mdc-auto-init', 'MDCTextField')
      .append($label)
      .append('<div class="mdc-line-ripple"></div>')
  }
}

// CHECKBOXES
function mdcCheckboxer(checkboxes) {
  // this gets complex but hold on. It'll make sense

  // store the labels in an array by order of appearance
  // start off my making sure we haven't already done this
  if ( $('.mdc-form-field', checkboxes).length <= 0 ) {
    // now that we know this is the first time...
    let labels = [] // initialize empty variable for storing the labels
    const $wrap = $(checkboxes).parent() // grap the input wrapper

    $('.mktoAsterix', $wrap).remove() // remove the astersisk

    // marketo does dual labels. this is the big label for the group of checkboxes
    const bigLabel = {
      for: $('label', $wrap).attr('for'), // important for accessibilit and interactivity 
      text: $('label', $wrap).html(), // gets the text value of the label
    }

    // stash the labels
    $('label', checkboxes).each(function(index) {
      // since we're calling $(this) more than once, this saves us processing time by cacheing that lookup
      let $this = $(this)

      // creating an object of this specific label
      const assembledLabel = {
        order: index, // important for iterating
        for: $this.attr('for'), // see ${bigLabel}
        text: $this.html() // see ${bigLabel}
      }

      labels.push(assembledLabel) // append this object to the ${labels} array
      $this.remove() // now that it's added, delete it from the DOM
    })

    // time to go through the checkboxes
    $('input', checkboxes).each(function(index) {
      // since we're calling $(this) more than once, this saves us processing time by cacheing that lookup
      let $this = $(this)

      $this
        .addClass('mdc-checkbox__native-control') // add MDC classes
        .wrap('<div class="mdc-form-field" />') // create a parent div
        .wrap('<div class="mdc-checkbox" />') // nest another div above the input and below the parent div

      // create two vars based on the above work
      let $mdc_checkbox = $this.parent() // <div class="mdc-checkbox"></div>
      let $mdc_form_field = $this.parent().parent() // <div class="mdc-form-field"></div>

      // make mdc adjustments to the checkbox
      $mdc_checkbox
        .attr('data-mdc-auto-init', 'MDCCheckbox') // lets MDC auto init on this
        .append(
          // we add a whole chunk of html here to animate those checkmarks.
          `
            <div class="mdc-checkbox__background">
              <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                <path class="mdc-checkbox__checkmark-path" fill="none"  d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
              </svg>
              <div class="mdc-checkbox__mixedmark"></div>
            </div>
          `
      )

      $mdc_form_field.attr('data-mdc-auto-init', 'MDCFormField') // auto init for the form-field to allow label clicking

      
      // Marketo labels behave differently if they have one or more than one, so here we account for that
      if ( labels.length > 1 ) {
        // add all the mini labels if the checkboxes were plural
        $mdc_form_field.append(`<label data-here="x" for="${labels[index].for}">${labels[index].text}</label>`)
      } else {
        // if they're singular, add the bigLabel instead
        $('label', $wrap).remove()
        $mdc_form_field.append(`<label data-here="x" for="${bigLabel.for}">${bigLabel.text}</label>`)
        
      }
    })
  }
}

// RADIO BUTTONS
function mdcCRadioButtonizer(radioButtons) {

  // Read the mdcCheckBoxer() notes to understand. This is nearly identical.
  if ( $('.mdc-form-field', radioButtons).length <= 0 ) {
    const $fieldWrap = $(radioButtons).parent()
    $('.mktoAsterix', $fieldWrap).remove()
    let labels = []
    const bigLabel = $('.mktoLabel', $fieldWrap).html()

    $('.mktoLabel', $fieldWrap).remove()

    $('label', radioButtons).each(function(index){
      const assembledLabel = {
        order: index,
        for: $(this).attr('for'),
        text: $(this).html()
      }
      labels.push(assembledLabel)
      $(this).remove();
    })

    $('input', radioButtons).each(function(index){
      let $this = $(this)

      $this
        .addClass('mdc-radio__native-control')
        .wrap('<div class="mdc-form-field" />')
        .wrap('<div class="mdc-radio" />')
        
      let $mdcRadio = $(this).parent()
      
      $mdcRadio
        .attr('data-mdc-auto-init', 'MDCRadio')
        .append(
        `
          <div class="mdc-radio__background">
            <div class="mdc-radio__outer-circle"></div>
            <div class="mdc-radio__inner-circle"></div>
          </div>
        `
      )

      $mdcRadio.parent()
        .attr('data-mdc-auto-init', 'MDCFormField')
        .append(`<label for="${labels[index].for}">${labels[index].text}</label>`)
    })

    $fieldWrap.prepend(`<label class='mktoLabel'>${bigLabel}</label>`)
  }
}

// SELECT DROPDOWN
function mdcSelectivizer(dropdown) {
  if ( ! $(dropdown).hasClass('mdc-select__native-control') ) {
    // set our vars and delete the asterisk
    const fieldWrap = $(dropdown).parent()
    $('.mktoAsterix', fieldWrap).remove()
    const label = $('label', fieldWrap)

    $('label', fieldWrap).remove() // remove the generated label for reconstruction

    // add that beatiful mdc class to the dropdown
    $(dropdown).addClass('mdc-select__native-control')

    // add that beatiful mdc class to the label as well
    $(label).addClass('mdc-floating-label mdc-floating-label--float-above')

    // tweak the wrapping div
    $(fieldWrap)
      .addClass('mdc-select') // mdc required class
      .append(label) // add the label back in
      .append('<div class="mdc-line-ripple"></div>') // make it ripple with love
      .attr('data-mdc-auto-init', 'MDCSelect') // auto init magic
  }

}

// BUTTONS
function mdcButtonizer(button) {
  $(button)
    .addClass('mdc-button mdc-button--unelevated') // required mdc classes
    .attr('data-mdc-auto-init', 'MDCRipple') // make that button ripple
}

// don't do any of this until your form is rendered!
MktoForms2.onFormRender(function (form) {

  // remove the extra Marketo things
  // Marketo is bloated with extra stylesheets and divs we don't needs so let's delete em
  $('#mktoForms2BaseStyle, #mktoForms2ThemeStyle, .mktoClear, .mktoOffset, .mktoGutter').remove()

  // collect the different inputs
  let $mktoTextFields = $('.mktoTextField, .mktoEmailField, .mktoNumberField, .mktoTelField, .mktoUrlField, .mktoDateField')
  let $mktoTextAreas = $('textarea.mktoField')
  let $mktoCheckboxes = $('.mktoCheckboxList')
  let $mktoRadioButtons = $('.mktoRadioList')
  let $mktoSelects = $('.mktoFieldWrap select')
  let $mktoButtons = $('.mktoButton')
  
  // work magic on the inputs
  $mktoTextFields.each( function() { mdcTextInputizer( this ) })
  $mktoTextAreas.each( function() { mdcTextAreaizer( this ) })
  $mktoCheckboxes.each( function() { mdcCheckboxer( this ) })
  $mktoRadioButtons.each( function() { mdcCRadioButtonizer( this ) })
  $mktoSelects.each( function() { mdcSelectivizer( this ) })
  $mktoButtons.each( function() { mdcButtonizer( this ) } )

  // the easy way of firing material.
  mdc.autoInit()
})    