const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin')
const webpack = require('webpack')
const { currentEditor } = require('./project-variables')
const moment = require('moment')

const currentDate = moment()

function allPages() {
  for (page in pages) {
  
  }



}

module.exports = {
  entry: {
    app: './src/index.js',
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, '../dist')
  },
  plugins: [
    // set all new pages here with a new HtmlWebPackPlugin() declaration
    new HtmlWebpackPlugin(      
      {
        title: 'Index',
        filename: 'index.html',
        template: 'src/index.html',
        inject: false,
      }
    ),
    /*
    new HtmlWebpackPlugin(      
      {
        title: 'Second Page',
        filename: 'second.html',
        template: 'src/second.html',
        inject: false,
      }
    ),
    */
    new CopyWebpackPlugin([
      { from: './src/images', to: 'images' }
    ]),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),
    new HtmlReplaceWebpackPlugin([
      {
        pattern: '@editor-data',
        replacement: `Edited by ${currentEditor} on ${currentDate}.`
      }
    ])
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
}