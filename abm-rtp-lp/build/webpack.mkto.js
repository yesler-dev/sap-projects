const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const autoprefixer = require('autoprefixer')
const { uniqueID, mktoInstance } = require('./project-variables.js')

module.exports = merge(common, {
  output: {
    filename: `${uniqueID}-[name].bundle.js`,
    path: path.resolve(__dirname, '../mkto-dist')
  },
  mode: 'production',
  devtool: 'source-map',
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: true
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },  
  plugins: [
    new CleanWebpackPlugin(
      ['mkto-dist'], 
      { root: path.resolve(__dirname, '../')}
    ),
    new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: `${uniqueID}-[name].css`,
        chunkFilename: "[id].css"
    }),
    new HtmlReplaceWebpackPlugin([
      {
        pattern: '<link rel="stylesheet" href="app.css">',
        replacement: `<link rel="stylesheet" href="${mktoInstance}/${uniqueID}-app.css">`
      },
      {
        pattern: '<script src="app.bundle.js"></script>',
        replacement: `<script src="${mktoInstance}/${uniqueID}-app.bundle.js"></script>`
      },
      {
        pattern: '<img src="images/',
        replacement: `<img src="${mktoInstance}/`
      }
    ])
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader, 
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [autoprefixer()]
            }
          }, 
          { 
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              includePaths: ['./node_modules']
            }
          }
        ]
      }
    ]
  }
});