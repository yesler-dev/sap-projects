const app = new Vue ({
    el: '#app',
    delimiters: ['${', '}'],
    data: function() {
        return {
            search: '',
            "participants": [
  {
    "First Name": "Piya",
    "Last Name": "Atmungkun",
    "Job Title": "President",
    "Company Name": "CATC (Civil Aviation Training Centre)",
    "Headshot Upload": "bangkok-piya-atmungkun.jpg"
  },
  {
    "First Name": "Mary Jo A.",
    "Last Name": "Bernardo-Aragon",
    "Job Title": "Ambassador Extraordinary and Plenipotentiary of the Philippines",
    "Company Name": "The Embassy of the Philippines",
    "Headshot Upload": "bangkok-maryjo-bernardo-aragon.jpg"
  },
  {
    "First Name": "Dr. Wan Abdul Hannan",
    "Last Name": "Bin Wan Ibadullah",
    "Job Title": "Co-founder and CEO",
    "Company Name": "Hospitals Beyond Boundaries (HBB)",
    "Headshot Upload": "bangkok-wan-hannan.jpg"
  },
  {
    "First Name": "Felix Ali",
    "Last Name": "Chendra",
    "Job Title": "Director",
    "Company Name": "Lippo Group",
    "Headshot Upload": "bangkok-felix-chendra.jpg"
  },
  {
    "First Name": "Suwat",
    "Last Name": "Chiochanchai",
    "Job Title": "Deputy Governor for ICT and CIO",
    "Company Name": "Provincial Electricity Authority (PEA)",
    "Headshot Upload": "bangkok-suwat-chiochanchai.jpg"
  },
  {
    "First Name": "Donald",
    "Last Name": "Choi",
    "Job Title": "CEO",
    "Company Name": "Chinachem Group",
    "Headshot Upload": "bangkok-donald-choi.jpg"
  },
  {
    "First Name": "Rosaline",
    "Last Name": "Chow Koo",
    "Job Title": "Founder and CEO",
    "Company Name": "CXA Group",
    "Headshot Upload": "bangkok-rosaline-chow-koo.jpg"
  },
  {
    "First Name": "Ernest",
    "Last Name": "Cu",
    "Job Title": "President and CEO",
    "Company Name": "Globe Telecom, Inc.",
    "Headshot Upload": "bangkok-ernest-cu.jpg"
  },
  {
    "First Name": "Sugiharto",
    "Last Name": "Darmakusuma",
    "Job Title": "CEO",
    "Company Name": "PT Mega Akses Persada",
    "Headshot Upload": "bangkok-sugiharto-darmakusuma.jpg"
  },
  {
    "First Name": "Ferdinand \"Ferdz\" M.",
    "Last Name": "Dela Cruz",
    "Job Title": "President and CEO",
    "Company Name": "Manila Water Company",
    "Headshot Upload": "bangkok-ferdinand-delacruz.jpg"
  },
  {
    "First Name": "Brent",
    "Last Name": "Eastwood",
    "Job Title": "CEO",
    "Company Name": "JBS Australia",
    "Headshot Upload": "bangkok-brent-eastwood.jpg"
  },
  {
    "First Name": "Sochivin",
    "Last Name": "Hang",
    "Job Title": "Commercial Counselor",
    "Company Name": "The Royal Embassy of Cambodia, Bangkok",
    "Headshot Upload": "bangkok-hang-sochivin.jpg"
  },
  {
    "First Name": "Hiroshi",
    "Last Name": "Ishino",
    "Job Title": "President",
    "Company Name": "Kansai Paint Co.,Ltd.",
    "Headshot Upload": "bangkok-hiroshi-ishino.jpg"
  },
  {
    "First Name": "John",
    "Last Name": "Kerry",
    "Job Title": "68th Secretary of State of the United States (2013-2017)",
    "Company Name": "",
    "Headshot Upload": "bangkok-john-kerry.jpg"
  },
  {
    "First Name": "Van",
    "Last Name": "Le",
    "Job Title": "Co-founder and Chief Strategy & Innovation Officer",
    "Company Name": "Xinja",
    "Headshot Upload": "bangkok-le-van.jpg"
  },
  {
    "First Name": "In-Young",
    "Last Name": "Lee",
    "Job Title": "CEO",
    "Company Name": "Hanon Systems",
    "Headshot Upload": "bangkok-in-young-lee.jpg"
  },
  {
    "First Name": "Chin Hua",
    "Last Name": "Loh",
    "Job Title": "CEO",
    "Company Name": "Keppel Corporation Limited",
    "Headshot Upload": "bangkok-chinahua-loh.jpg"
  },
  {
    "First Name": "Bill ",
    "Last Name": "McDermott",
    "Job Title": "CEO",
    "Company Name": "SAP",
    "Headshot Upload": "bangkok-bill-mcdermott.jpg"
  },
  {
    "First Name": "Krisda",
    "Last Name": "Monthienvichienchai",
    "Job Title": "President and CEO",
    "Company Name": "Mitr Phol Sugar Corp., Ltd",
    "Headshot Upload": "bangkok-krisda-monthienvichienchai.jpg"
  },
  {
    "First Name": "Lisa",
    "Last Name": "Oake",
    "Job Title": "Founder and CEO",
    "Company Name": "Oake Media",
    "Headshot Upload": "bangkok-lisa-oake.jpg"
  },
  {
    "First Name": "Arawadee",
    "Last Name": "Photisaro",
    "Job Title": "Managing Director",
    "Company Name": "PTT Digital Solution Company Limited",
    "Headshot Upload": "bangkok-arawadee-photisaro.jpg"
  },
  {
    "First Name": "Toby",
    "Last Name": "Ruckert",
    "Job Title": "CEO",
    "Company Name": "UIB Holdings",
    "Headshot Upload": "bangkok-toby-ruckert.jpg"
  },
  {
    "First Name": "Scott",
    "Last Name": "Russell",
    "Job Title": "President, APJ",
    "Company Name": "SAP",
    "Headshot Upload": "bangkok-scott-russell.jpg"
  },
  {
    "First Name": "Yul",
    "Last Name": "Ryu",
    "Job Title": "President",
    "Company Name": "S-OIL",
    "Headshot Upload": "bangkok-yul-ryu.jpg"
  },
  {
    "First Name": "Bambang Widjanarko",
    "Last Name": "Santoso",
    "Job Title": "CEO",
    "Company Name": "PT Astra International, TBK",
    "Headshot Upload": "bangkok-paulus-widjanarko.jpg"
  },
  {
    "First Name": "Jan",
    "Last Name": "Scheer",
    "Job Title": "Chargé d’Affaires a.i.",
    "Company Name": "Embassy of the Federal Republic of Germany",
    "Headshot Upload": "bangkok-jan-scheer.jpg"
  },
  {
    "First Name": "Pansak",
    "Last Name": "Siriruchatapong, D.Ing",
    "Job Title": "Vice Minister of Digital Economy and Society",
    "Company Name": "Kingdom of Thailand",
    "Headshot Upload": "bangkok-pansak-siriruchatapong.jpg"
  },
  {
    "First Name": "Chatri",
    "Last Name": "Sityodtong",
    "Job Title": "Founder, Chairman and CEO",
    "Company Name": "ONE Championship",
    "Headshot Upload": "bangkok-chatri-sityodtong.jpg"
  },
  {
    "First Name": "Elaine",
    "Last Name": "Tan",
    "Job Title": "Executive Director",
    "Company Name": "ASEAN Foundation",
    "Headshot Upload": "bangkok-elaine-tan.jpg"
  },
  {
    "First Name": "Ernesto",
    "Last Name": "Tanmantiong",
    "Job Title": "President, CEO & Executive Director",
    "Company Name": "Jollibee Foods Corporation",
    "Headshot Upload": "bangkok-ernesto-tanmantiong.jpg"
  },
  {
    "First Name": "Piti",
    "Last Name": "Tantakasem",
    "Job Title": "CEO",
    "Company Name": "TMB Bank Public Co., Ltd",
   "Headshot Upload": "bangkok-piti-tantakasem.jpg"
  },
  {
    "First Name": "Win Win",
    "Last Name": "Tint",
    "Job Title": "Chief Executive Officer",
    "Company Name": "City Mart Holding Co., Ltd.",
    "Headshot Upload": "bangkok-win-win-tint.jpg"
  },
  {
    "First Name": "Chansin",
    "Last Name": "Treenuchagron",
    "Job Title": "President and CEO",
    "Company Name": "PTT Group",
    "Headshot Upload": "bangkok-chansin-treenuchagron.jpg"
  },
  {
    "First Name": "Nick",
    "Last Name": "Tzitzon",
    "Job Title": "EVP, Marketing and Communications",
    "Company Name": "SAP",
    "Headshot Upload": "bangkok-nick-tzitzon.jpg"
  },
 {
   "First Name": "Kunal",
   "Last Name": "Upadhyay",
   "Job Title": "Managing Partner",
   "Company Name": "Bharat Innovation Fund and Chief Executive, Centre for Innovation Incubation and Entrepreneurship",
   "Headshot Upload": "bangkok-kunal-upadhyay.jpg"
 },
 {
   "First Name": "Dennis",
   "Last Name": "Van Heezik",
   "Job Title": "CEO",
   "Company Name": "INSEE Digital Company Limited",
   "Headshot Upload": "bangkok-dennis-vanheezik.jpg"
 },
  {
    "First Name": "Alan",
    "Last Name": "Wilson",
    "Job Title": "Regional CEO",
    "Company Name": "MSIG Holdings (Asia) Pte Ltd",
    "Headshot Upload": "bangkok-alan-wilson.jpg"
  },
  {
    "First Name": "Heang Fine",
    "Last Name": "Wong",
    "Job Title": "Group CEO",
    "Company Name": "Surbana Jurong Private Limited",
    "Headshot Upload": "bangkok-heangfine-wong.jpg"
  }
]
        };
    },
    computed: {
        filteredList: function() {
            let parts_arr = this.participants,
                searchString = this.search;
            if(!searchString) {
                return parts_arr;
            }

            searchString = searchString.trim().toLowerCase();

            parts_arr = parts_arr.filter(function(part) {
                if(part['First Name'].toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part['Last Name'].toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part['Job Title'].toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part['Company Name'].toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                }
            })
            return parts_arr;
        }
    }
});