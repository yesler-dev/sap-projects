const app = new Vue({
  el: '#app',
  delimiters: ['${', '}'],
  data: function () {
    return {
      search: '',
      list: ['one', 'two'],
      "participants": [
        {
          firstName: "Yousef",
          lastName: "Abdullah Al-Benyan",
          company: "Saudi Basic Industries",
          title: "CEO and Acting Vice Chairman",
          headshot: "mar-al-benyan.jpg",
          guest: ""
        },
        {
          firstName: "Silas",
          lastName: "Adekunle",
          company: "Reach Robotics",
          title: "Co-founder and CEO",
          guest: "Zeynep Findik",
          headshot: "marbella-silas-adekunle.jpg",
          "isSpeaker": true,
        },
        {
          firstName: "His Excellency Khadim",
          lastName: "Al Darei",
          company: "Al Dahra Holding",
          title: "CEO",
          headshot: "marbella-khadim-al-darei.jpg",
        },
        {
          firstName: "Nasser",
          lastName: "Al-Kuwari",
          company: "Qatar Chemical Company",
          title: "CEO",
          headshot: "marbella-nasser-al-kuwari.jpg",
        },
        {
          firstName: "His Excellency Omar",
          lastName: "Al Olama",
          company: "",
          title: "Minister of State for Artificial Intelligence",
          guest: "",
          headshot: "marbella-omar-al-olama.jpg",
        },
        {
          firstName: "Adel",
          lastName: "Ali",
          title: "Group CEO",
          company: "Air Arabia",
          guest: ""
        },
        {
          firstName: "Mutlaq",
          lastName: "Al-Morished",
          company: "Tasnee",
          title: "CEO",
          headshot: "marbella-mutlaq-almorished.jpg",
        },
        {
          firstName: "Khaled",
          lastName: "Attia",
          title: "CEO",
          company: "TransIT",
          guest: ""
        },
        {
          firstName: "His Excellency Khalfan",
          lastName: "Belhoul",
          company: "Dubai Future Foundation",
          title: "CEO",
        },
        {
          firstName: "Joseph",
          lastName: "Benito",
          company: "Seidor",
          title: "Co-CEO and Founder",
          guest: ""
        },
        {
          firstName: "Tomaž",
          lastName: "Berločnik",
          company: "Petrol d.d.",
          title: "CEO",
          guest: "Maja Berlocnik",
          headshot: "mar-tomaz.jpg",
        },
        {
          firstName: "Oliver",
          lastName: "Bierhoff",
          company: "German National Football Team / Academy DFB",
          title: "General Manager",
          guest: "Klara Bierhoff",
        },
        {
          firstName: "Marco",
          lastName: "Bizzarri",
          title: "CEO",
          company: "Gucci",
          guest: ""
        },
        {
          firstName: "Stephan",
          lastName: "Borchert",
          company: "Grandvision",
          title: "CEO",
        },
        {
          firstName: "Kaya",
          lastName: "Busch",
          company: "Busch Dienste GmbH",
          title: "CEO",
          guest: "Nihan Busch",
          headshot: "marbella-kaya-busch.jpg",
        },
        {
          firstName: "Nabil",
          lastName: "Bustros",
          company: "Midis Group",
          title: "Chairman",
          guest: "Carole Bustros",
          headshot: "mar-bustros.jpg",
        },
        {
          firstName: "Bernard",
          lastName: "Byrne",
          company: "Davy Group",
          title: "Deputy Chief Executive",
          guest: "Fiona Byrne",
          headshot: "mar-byrne.jpg",
        },
        {
          firstName: "Christian",
          lastName: "Chammas",
          company: "Vivo Energy",
          title: "CEO",
          guest: "Erica Chammas",
          headshot: "mar-chammas.jpg",
        },
        {
          firstName: "Darren",
          lastName: "Davis",
          company: "Maaden",
          title: "President and CEO",
          guest: "Keiko Davis",
          headshot: "mar-davis.jpg",
        },
        {
          firstName: "Paolo",
          lastName: "De Cesare",
          company: "Printemps",
          title: "CEO",
          guest: "",
          headshot: "marbella-printemps-decesare.jpg",
        },
        {
          firstName: "Philippe",
          lastName: "Delpech",
          company: "Sonepar",
          title: "CEO",
          guest: "Kristina Delpech",
          headshot: "mar-delpech.jpg",
        },
        {
          firstName: "Johan",
          lastName: "Dennelind",
          company: "Telia Sonera",
          title: "President and CEO",
          guest: "",
          isSpeaker: true,
          headshot: "marbella-johan-dennelind.jpg",
        },
        {
          firstName: "Tony",
          lastName: "Douglas",
          title: "CEO",
          company: "Etihad Airways",
          guest: ""
        },
        {
          firstName: "Juliàn",
          lastName: "Díaz González",
          company: "Dufry",
          title: "CEO",
          guest: "Milagros Muñoz",
        },
        {
          firstName: "Jean-Marc",
          lastName: "Dublanc",
          company: "ADISSEO",
          title: "CEO",
          guest: "Sylvia Dublanc",
        },
        {
          firstName: "Fatih",
          lastName: "Ebiçlioğlu",
          company: "Arcelik (Koç Holding)",
          title: "Durable Goods Group President",
          guest: "",
          headshot: "marbella-kemal-fatih-ebiclioglu.jpg",
        },
        {
          firstName: "Ahmed",
          lastName: "El Sewedy",
          company: "El Sewedy",
          title: "CEO",
          guest: ""
        },
        {
          firstName: "Enrique",
          lastName: "Fernandez",
          title: "CEO",
          company: "M.Video Management",
          guest: ""
        },
        {
          firstName: "Carl",
          lastName: "Grivner",
          company: "Colt Technology Services",
          title: "CEO",
          headshot: "marbella-carl-grivner.jpg",
          guest: "Kaye Phares"
        },
        {
          firstName: "Bear",
          lastName: "Grylls",
          title: "Adventurer, Author, and Television Host",
          company: "",
          guest: "",
          headshot: "marbella-bear-grylls.jpg",
          "isSpeaker": true,
        },
        {
          firstName: "Frank",
          lastName: "Hiller",
          company: "Deutz AG",
          title: "CEO",
        },
        {
          firstName: "Klaus",
          lastName: "Hofmann",
          company: "Minimax",
          title: "CEO",
        },
        {
          firstName: "Thomas",
          lastName: "Kaeser",
          title: "CEO",
          company: "Kaeser Kompressoren",
          guest: "Tina-Marie Kaeser",
          headshot: "marbella-thomas-kaeser.jpg"
        },
        {
          firstName: "Pakinam",
          lastName: "Kafafi",
          title: "CEO",
          company: "TAQA Arabia",
          guest: "",
          headshot: "marbella-pakinam-kafafi.jpg",
        },
        {
          firstName: "Andreas",
          lastName: "Klein",
          title: "CEO",
          company: "Döhler GmbH",
          guest: ""
        },
        {
          firstName: "Reinhard",
          lastName: "Klein",
          title: "CEO",
          company: "Bausparkasse Schwäbisch Hall",
          guest: "Catja Klein"
        },
        {
          firstName: "Christoph",
          lastName: "Klenk",
          title: "CEO",
          company: "Krones AG",
          guest: ""
        },
        {
          firstName: "Nish",
          lastName: "Kankiwala",
          company: "Hovis",
          title: "CEO",
          guest: "Natalie Walsh",
          headshot: "mar-kankiwala.jpg",
        },
        {
          firstName: "Stefan",
          lastName: "Klebert",
          company: "GEA",
          title: "CEO",
          guest: "Géraldine Maeker",
        },
        {
          firstName: "Jochen",
          lastName: "Kress",
          company: "MAPAL",
          title: "President",
          headshot: "marbella-jochen-kress.jpg",
          guest: "Anja Kress"
        },
        {
          firstName: "Alexander",
          lastName: "Lichtenberg",
          company: "Bausparkasse Schwäbisch Hall",
          title: "Managing Director",
          guest: "Emine Shain"
        },
        {
          firstName: "Andrea Alberto",
          lastName: "Lovato",
          company: "Tenova",
          title: "CEO",
          guest: "Cecilia Santos Escolano",
        },
        {
          firstName: "Martin",
          lastName: "Lundstedt",
          company: "Volvo Group",
          title: "President and CEO",
          guest: "Eva Lundstedt",
          "isSpeaker": true,
          headshot: "mar-lundstedt.jpg",
        },
        {
          firstName: "Wolfgang",
          lastName: "Marguerre",
          company: "Octapharma AG",
          title: "Chairman",
          guest: "Barbara Marguerre",
        },
        {
          firstName: "Luis",
          lastName: "Maroto",
          company: "Amadeus",
          title: "CEO",
          guest: "Cristina Pinel Calamita",
          headshot: "mar-maroto.jpg",
        },
        {
          firstName: "Bill",
          lastName: "McDermott",
          company: "SAP",
          title: "CEO",
          headshot: "mcdermott.jpg",
          "isSpeaker": "true",
          guest: "Julie McDermott"
        },
        {
          firstName: "Henry",
          lastName: "McGovern",
          company: "AmRest",
          title: "CEO",
          headshot: "mar-mcgovern.jpg",
        },
        {
          firstName: "James",
          lastName: "Mworia",
          company: "Centum Investments",
          title: "CEO",
          guest: "Joan Mworia",
        },
        {
          firstName: "Harald",
          lastName: "Neumann",
          title: "CEO",
          company: "Novomatic Gruppe",
          guest: ""
        },
        {
          firstName: "Barack",
          lastName: "Obama",
          title: "44th President of the United States",
          company: "",
          headshot: "marbella-obama.jpg",
          "isSpeaker": "true",
        },
        {
          firstName: "Melissa",
          lastName: "Odabash",
          company: "Melissa Odabash",
          title: "Founder and CEO",
          headshot: "marbella-melissa-odabash.jpg",
        },
        {
          firstName: "Claudio Boada",
          lastName: "Pallerés",
          title: "Chairman",
          company: "AEGON",
          guest: "Beatriz Suarez"
        },
        {
          firstName: "Chemi",
          lastName: "Peres",
          title: "Managing General Partner and Co-Founder",
          company: "Pitango Venture Capital",
          guest: ""
        },
        {
          firstName: "Marc",
          lastName: "Puig",
          title: "Chairman and CEO",
          company: "Puig",
          guest: ""
        },
        {
          firstName: "Lee",
          lastName: "Ranson",
          company: "Eversheds Sutherland",
          title: "CEO",
          guest: "Alexis Ranson"
        },
        {
          firstName: "Stjepan",
          lastName: "Roglic",
          company: "Orbico Group",
          title: "CEO",
          guest: "Larisa Roglic",
          headshot: "mar-roglic.jpg",
        },
        {
          firstName: "Omar",
          lastName: "Safey El Din",
          company: "EGIC",
          title: "CEO",
          guest: ""
        },
        {
          firstName: "Rob",
          lastName: "Schilder",
          company: "G-Star RAW C.V.",
          title: "CEO",
          guest: "Ester Boer"
        },
        {
          firstName: "Staffan",
          lastName: "Schüberg",
          title: "CEO",
          company: "Esteve Pharmaceuticals",
          guest: "Lidia Schüberg"
        },
        {
          firstName: "Florian",
          lastName: "Seiche",
          company: "HMD Global",
          title: "CEO",
          guest: "",
          headshot: "marbella-florian-seiche.jpg",
        },
        {
          firstName: "Jaan Ivar",
          lastName: "Semlitsch",
          company: "Elkjop",
          title: "CEO",
          guest: "",
          headshot: "mar-semlitsch.jpg",
        },
        {
          firstName: "Risto",
          lastName: "Siilasmaa",
          company: "Nokia",
          title: "Chairman",
          guest: "",
          headshot: "mar-siilasmaa.jpg",
        },
        {
          firstName: "Emma",
          lastName: "Smith",
          company: "Eversend",
          title: "Cofounder and COO",
          guest: "",
          isSpeaker: true
        },
        {
          firstName: "Osman",
          lastName: "Sultan",
          company: "DU",
          title: "CEO",
          guest: "Ghada Elsokkari",
          headshot: "mar-sultan.jpg",
        },
        {
          firstName: "Rajeev",
          lastName: "Suri",
          company: "Nokia",
          title: "CEO",
          guest: "",
          headshot: "marbella-rajeev-suri.jpg",
        },
        {
          firstName: "Seifeldin",
          lastName: "Thabet",
          company: "Juhayna",
          title: "CEO",
          guest: ""
        },
        {
          firstName: "Harry",
          lastName: "Thomsen",
          company: "SAP",
          title: "President MEE",
          guest: ""
        },
        {
          firstName: "Maxim",
          lastName: "Timchenko",
          company: "DTEK, LLC",
          title: "CEO",
          guest: ""
        },
        {
          firstName: "Andrea",
          lastName: "Zappia",
          company: "Sky Italia",
          title: "CEO",
          guest: "Sara Dethridge"
        },
         
      ]
    }
  },
  computed: {
    filteredList: function () {
      let parts_arr = this.participants,
                searchString = this.search;
            if(!searchString) {
                return parts_arr;
            }

            searchString = searchString.trim().toLowerCase();

            parts_arr = parts_arr.filter(function(part) {
                if(part.firstName.toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part.lastName.toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part.title.toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                } else if(part.company.toLowerCase().indexOf(searchString) !== -1) {
                    return part;
                }
            })
            return parts_arr;
    }
  }
})