  {
    "First Name": "Yousef",
    "Last Name": "Abdullah Al-Benyan",
    "Company Name": "Saudi Basic Industries",
    "Job Title": "CEO & Acting Vice Chairman",
    "Guest": ""
  },
  {
    "First Name": "Andrea",
    "Last Name": "Alberto Lovato",
    "Company Name": "Tenova",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Mutlaq",
    "Last Name": "Al-Morished",
    "Company Name": "Tasnee",
    "Job Title": "CEO",
    "Guest": "Lama AlSaleh"
  },
  {
    "First Name": "Gabriele",
    "Last Name": "Benedetto",
    "Company Name": "Telepass",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Joseph",
    "Last Name": "Benito",
    "Company Name": "Seidor",
    "Job Title": "Co-Ceo & Founder",
    "Guest": ""
  },
  {
    "First Name": "Tomaž ",
    "Last Name": "Berlocnik",
    "Company Name": "Petrol d.d.",
    "Job Title": "CEO",
    "Guest": "Maja Berlocnik"
  },
  {
    "First Name": "Oliver",
    "Last Name": "Bierhoff",
    "Company Name": "DFB",
    "Job Title": "General Manager German National Football Team/Academy",
    "Guest": "Klara Bierhoff"
  },
  {
    "First Name": "Stephan",
    "Last Name": "Borchert",
    "Company Name": "Grandvision",
    "Job Title": "CEO",
    "Guest": "Mrs. Borchert"
  },
  {
    "First Name": "Nabil",
    "Last Name": "Bustros",
    "Company Name": "Midis Group",
    "Job Title": "Chairman",
    "Guest": "Carole Bustros"
  },
  {
    "First Name": "Bernard",
    "Last Name": "Byrne",
    "Company Name": "Davy Group",
    "Job Title": "Deputy Chief Executive",
    "Guest": "Fiona Byrne"
  },
  {
    "First Name": "Christian",
    "Last Name": "Chammas",
    "Company Name": "Vivo Energy",
    "Job Title": "CEO",
    "Guest": "Erica Chammas"
  },
  {
    "First Name": "Darren",
    "Last Name": "Davis",
    "Company Name": "Maaden",
    "Job Title": "President & CEO",
    "Guest": ""
  },
  {
    "First Name": "Paolo",
    "Last Name": "De Cesare",
    "Company Name": "Printemps",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Philippe",
    "Last Name": "Delpech",
    "Company Name": "Sonepar",
    "Job Title": "CEO",
    "Guest": "Kristina Delpech"
  },
  {
    "First Name": "Johan",
    "Last Name": "Dennelind",
    "Company Name": "Telia Sonera",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Juliàn",
    "Last Name": "Díaz González",
    "Company Name": "Dufry",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Fatih",
    "Last Name": "Ebiclioglu",
    "Company Name": "Arcelik (Koç Holding)",
    "Job Title": "Durable Goods Group President",
    "Guest": ""
  },
  {
    "First Name": "Ahmed",
    "Last Name": "EL Sewedy",
    "Company Name": "El Sewedy",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Carl",
    "Last Name": "Grivner",
    "Company Name": "Colt Technology Services",
    "Job Title": "CEO",
    "Guest": "Kaye Phares"
  },
  {
    "First Name": "Frank",
    "Last Name": "Hiller",
    "Company Name": "Deutz AG",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Nish",
    "Last Name": "Kankiwala",
    "Company Name": "Hovis",
    "Job Title": "CEO",
    "Guest": "Natalie Walsh"
  },
  {
    "First Name": "Stefan",
    "Last Name": "Klebert",
    "Company Name": "GEA",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Jochen",
    "Last Name": "Kress",
    "Company Name": "MAPAL",
    "Job Title": "President",
    "Guest": "Anja Kress"
  },
  {
    "First Name": "Martin",
    "Last Name": "Lundstedt",
    "Company Name": "Volvo Group",
    "Job Title": "President & CEO",
    "Guest": "Eva Lundstedt"
  },
  {
    "First Name": "Luis",
    "Last Name": "Maroto",
    "Company Name": "Amadeus",
    "Job Title": "CEO",
    "Guest": "Cristina Pinel Calamita"
  },
  {
    "First Name": "Henry",
    "Last Name": "McGovern",
    "Company Name": "AmRest",
    "Job Title": "CEO",
    "Guest": "Gosia McGovern"
  },
  {
    "First Name": "Melissa",
    "Last Name": "Odabash",
    "Company Name": "Melissa Odabash",
    "Job Title": "Founder & CEO",
    "Guest": "Nicolas Desantis"
  },
  {
    "First Name": "Lee",
    "Last Name": "Ranson",
    "Company Name": "Eversheds Sutherland",
    "Job Title": "CEO",
    "Guest": "Alexis Ranson"
  },
  {
    "First Name": "Stjepan",
    "Last Name": "Roglic",
    "Company Name": "Orbico Grupa LTD",
    "Job Title": "CEO",
    "Guest": "Larisa Roglic"
  },
  {
    "First Name": "Omar",
    "Last Name": "Safey El Din",
    "Company Name": "EGIC",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Rob",
    "Last Name": "Schilder",
    "Company Name": "G-Star",
    "Job Title": "CEO",
    "Guest": "Mrs. Schilder"
  },
  {
    "First Name": "Florian",
    "Last Name": "Seiche",
    "Company Name": "HMD Global",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Jaan Ivar",
    "Last Name": "Semlitsch",
    "Company Name": "Elkjop",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Risto",
    "Last Name": "Siilasmaa",
    "Company Name": "Nokia",
    "Job Title": "Chairman",
    "Guest": ""
  },
  {
    "First Name": "Osman",
    "Last Name": "Sultan",
    "Company Name": "DU",
    "Job Title": "CEO",
    "Guest": "Ghada Elsokkari"
  },
  {
    "First Name": "Rajeev",
    "Last Name": "Suri",
    "Company Name": "Nokia",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Maxim",
    "Last Name": "Timchenko",
    "Company Name": "DTEK, LLC",
    "Job Title": "CEO",
    "Guest": ""
  },
  {
    "First Name": "Andrea",
    "Last Name": "Zappia",
    "Company Name": "Sky Italia",
    "Job Title": "CEO",
    "Guest": ""
  }