<div class="speakerDetails">
  <img src="./img/headshots/marbella-tony-douglas_.jpg" alt="Tony Douglas" class="fancyTrigger" data-src="#douglas">
  
  <div class="speaker-visible">
    <p><span class="fancyTrigger" data-src="#douglas"><strong>Tony Douglas</strong></span></p>
    <p>Group CEO, Etihad Aviation Group</p>
  </div>

  <div class="agendaBio" style="display: none;" id="douglas">
    <img src="./img/headshots/marbella-tony-douglas_.jpg" alt="Tony Douglas">
    <div class="modalDetails">
      <p style="margin-bottom: 0;">Tony Douglas</p>
      <p>Group CEO, Etihad Aviation Group</p>
      <div class="speakerBio">
<p>Tony Douglas joined Etihad Aviation Group as Group Chief Executive Officer in January 2018. He is responsible for fulfilling the Government of Abu Dhabi’s mandate for Etihad to operate safely, commercially and sustainably, by leading the seven divisions of the diversified global aviation and travel group. He has also led the Group’s restructuring and transformation programme, which has seen the airline improve its core operating performance by 34 per cent in the last two years.</p>

<p>Tony occupies Board positions as the Chairman of specialist UK engineering contractor Keltbray, where he has served since 2010. He is also a Board Director of Abu Dhabi Airports Company since 2018, supporting the mandate of developing international airport growth and the development of the 700,000 sqm Midfield Terminal.</p>

<p>Previously, Tony was Chief Executive Officer and UK National Armaments Director for the UK Ministry of Defence (DE&S), which he joined in September 2015. Here, he was responsible for Defence Equipment and Support to Her Majesty’s Armed Forces. Acting on behalf of the Army, Navy, Air Force and Joint Commands, DE&S is accountable for the delivery of £178bn of defence equipment requirements from mega projects such as QE Class Aircraft Carriers, Astute Submarines and F35 Joint Strike Fighters to the in-service support of all current assets.</p>

<p>Prior to this, Tony was Chief Executive of Abu Dhabi Airports which he joined in March 2013, a group comprising of five airports including Abu Dhabi international. The organisation is accountable for the development of $11bn of airport infrastructure in the Emirate of Abu Dhabi, including its crowning jewel, the iconic Midfield Terminal Building.</p>

<p>Tony joined Abu Dhabi Airports after completing the Khalifa Port and Industrial Zone mega project (KPIZ) which was delivered on schedule and below budget. As CEO of Abu Dhabi Ports Company (ADPC), since 2010, Tony was accountable for all operational sea ports in the region, during which time, record container growth was witnessed.</p>

<p>Tony also held the position of Chief Operating Officer and the Group Chief Executive designate for Laing O’Rourke, where he had a broad executive remit covering strategic business development and operational management across the Group’s three geographic hubs: Europe; Middle East and South Asia; and Australasia. As a member of the Group Executive Board, he had accountability for the Group’s largest and most complex project delivery activities.</p>

<p>Before joining Laing O’Rourke, he held a number of senior executive positions with BAA, the UK’s leading airport infrastructure operator and a FTSE 40 company, culminating in his appointment as Chief Executive in charge of Heathrow, one of the world’s premier airports. He was an executive member of the BAA Group Board and an active participant in the £16.3bn takeover of BAA by Grupo Ferrovial.</p>

<p>Tony was previously Heathrow Terminal 5 Managing Director, with overall executive responsibility for the delivery of the £4.3bn Terminal 5 build programme. Prior to this Mr. Douglas was BAA’s Group Technical Director with responsibility for technical functions, including development, design, group supply chain, construction and maintenance of new and improved airport facilities. He has also held the position as BAA’s Group Supply Chain Director accountable for a £1bn, annual expenditure, of construction and related products and services.</p>

<p>Earlier in his career he worked for the Kenwood Group as Manufacturing and Global Logistics Director. This was his first role on the board of a major internationally focused PLC. A mechanical engineer by training, his career began in 1979 at General Motors, where he joined as an apprentice industrial engineer. In 1990 he moved to BAe Systems, where he rose to become Product Manufacturing Director in its regional aircraft division, responsible for production of the “AVRO RJ” Regional Jet and previously the 64-seat ATP (Advanced Turbo Prop) Aircraft.</p>

      </div>
    </div>
  </div>
</div>