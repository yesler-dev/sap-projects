<div class="speakerDetails">
  <img src="./img/headshots/tomjones.jpg" alt="Sir Tom Jones" class="fancyTrigger" data-src="#tom">
  
  <div class="speaker-visible">
    <p><span class="fancyTrigger" data-src="#tom"><strong>Sir Tom Jones</strong></span></p>
    <p>Grammy Award Winner</p>
  </div>

  <div class="agendaBio" style="display: none;" id="tom">
    <img src="./img/headshots/tomjones.jpg" alt="Sir Tom Jones">
    <div class="modalDetails">
      <p style="margin-bottom: 0;">Sir Tom Jones</p>
      <p>Grammy Award Winner</p>
      <div class="speakerBio">

        <p>Thomas Jones Woodward was born in Pontypridd, South Wales, Great Britain on June 7, 1940 to a father who worked the coal mines of the Rhondda Valley. After quitting school at 15, working a variety of manual jobs, singing in the clubs at night and marrying at 17, Jones went on to sign with Decca Records in London.</p>

        <p>Jones has a fundamental interest in a wide range of music. Although he is well known for hits including It’s Not Unusual, Kiss, Delilah, What’s New Pussycat, I’ll Never Fall In Love Again and If I Only Knew, he is first and foremost an artist with a true rhythm and blues soul. He has remained a vital recording artist, with his 1999 album Reload the biggest selling (5m) of his then 35-year career. Tom was a key player in Martin Scorsese’s Red White & Blues series, and in 2004 released an album of roots rock n roll with Jools Holland. In 2008, 24 Hours, was a critical career highlight with many of the self-penned tracks being an insightful and evocative reflection of his spirited and rich life.</p>

        <p>2010 saw a change of direction with Tom releasing his 39th studio album, Praise & Blame. The Praise & Blame mantra was ‘get back to basics’, and the album was recorded as wholly live performances at Peter Gabriel’s studio in Bath, England, produced by Ethan Johns (Kings of Leon, Ryan Adams, Ray LaMontagne, Laura Marling, Paolo Nutini). The result witnessed a singer at the top of his game, with the songs themselves coaxing exhilarating performances out of Tom and his band.</p>

        <p>Following the success of Praise & Blame, in May 2012 Tom released ‘Spirit In The Room’. Paired once again with producer/guitarist Ethan Johns at Bath’s Real World Studios, ‘Spirit In The Room’, like it’s predecessor, allows an unvarnished Tom to bring a voice to songs as only he can. Accompanied by Ethan Johns throughout, the album includes gems from a diverse choice of writers - Richard Thompson, Leonard Cohen, Paul Simon and Paul McCartney amongst others. ‘Spirit In The Room’ is simple, raw and soulful.</p>

        <p>Tom was knighted by Her Majesty the Queen in 2006, an honour he deeply cherishes. An honour he was able to celebrate once again in 2012 when he performed at the Queens Diamond Jubilee concert at Buckingham Palace. Other highlights of his long career include receiving a BRIT Awards for Best Male and Outstanding Contribution to Music, a Silver Clef Award and a Silver Clef Award for Lifetime Achievement, the Hitmaker Award from the [US] Songwriters Hall of Fame, GQ Man Of The Year, and the prestigious [UK] Music Industry Trust Award. He has been animated as himself in The Simpsons, Duck Dodgers, The Emperors New Groove; other film roles include Tim Burton’s Mars Attacks. In 2012 he landed his first acting role not playing himself, alongside Brenda Blethyn and Alison Steadman, in “Playhouse Presents: King of The Teds” for Sky Arts.</p>

        <p>In and amongst a very successful touring year in 2014, Tom had the thrill of performing at the MCG in Melbourne for the AFL Grand Final, the premier sporting event in Australia, alongside Ed Sheeran. Later in the year, he had the honour of appearing at Neil Young’s Bridge School Benefit concert, sharing the bill with Florence + The Machine, Soundgarden, Pearl Jam, Brian Wilson and others. And over in the UK, he was pleased to close the first Annual BBC Music Awards, helping to salute a fantastic year of music across the BBC by performing alongside Paloma Faith, a chorus of hundreds and the BBC Concert Orchestra.</p>

        <p>In 2015, Sir Tom continued his role as Coach on The Voice UK, working alongside Will.i.am, Rita Ora and Ricky Wilson. In February, a very special invitation was received to perform at the 25th Anniversary of MusiCares Person Of The Year tribute honouring Bob Dylan, alongside musical luminaries Bruce Springsteen, Beck, Jack White, Bonnie Raitt, Willie Nelson and many more. The following evening he performed at the 57th Annual Grammy Awards. After a busy few months touring, Sir Tom released his first ever autobiography entitled Over The Top And Back (Penguin Books), and the book’s companion soundtrack entitled Long Lost Suitcase (VirginEMI/Caroline) which he actively promoted internationally. He also hosted and headlined a special 2-hour television event with his friends in music and comedy in a major fund-raising effort for the BBC’s charity Children In Need.</p>

        <p>2016 was a difficult but busy year of performing and touring for Sir Tom as he has laboured to work through the grief of losing his childhood friend and wife of 59 years, Melinda, in April. Tom and his band lifted performances to a different level, garnering rave reviews in Europe and America. A quick trip to Germany to perform with the stellar Helena Fischer finished the year on a positive note.</p>

        <p>2017 saw the return of Sir Tom to prime time Saturday night TV, resuming his role of coach and mentor on ITV’s The Voice UK, in which he guided new talent in the form of Welsh duo Into The Ark to the grand finale of the sixth series. The summer months brought a sell out tour and rave reviews as Sir Tom and his band toured the UK & Europe, bringing his Voice finalists Into The Ark out on the road as show openers and special guests.</p>

        <p>Tom is currently reprising his role as a coach on ITV’s The Voice, having returned after his huge success as the mentor for last years’ winner Ruti.</p>

        <p>Sir Tom is indeed a living legend, one of the few musical artists whose profession began at the dawn of modern popular music who continues to have a vital recording and performing career to this day. His irresistible show traverses musical eras and genres, cuts across class divides and appeals to young and old, male and female, mainstream and cutting edge. Sir Tom has always been about the power of the song, the power of the voice.</p>
      </div>
    </div>
  </div>
</div>