import IScroll from 'fullpage.js/vendors/scrolloverflow';
import fullpage from 'fullpage.js';
import lottie from 'lottie-web'
import enquire from 'enquire.js/dist/enquire'
import animationIntro from '../animations/intro.json'
import animationOne from '../animations/one.json'
import animationTwo from '../animations/two.json'
import animationThree from '../animations/three.json'
import animationOutro from '../animations/outro.json'
import './modernizr'

const $window = $(window)


const introAnim = lottie.loadAnimation({
  container: document.getElementById('intro-animation'), // the dom element that will contain the animation
  renderer: 'svg',
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationIntro
});

const anim1 = lottie.loadAnimation({
  container: document.getElementsByClassName('animation-one')[0], // the dom element that will contain the animation
  renderer: 'svg',
  loop: false,
  autoplay: false,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationOne
});

const anim2 = lottie.loadAnimation({
  container: document.getElementsByClassName('animation-two')[0], // the dom element that will contain the animation
  renderer: 'svg',
  loop: false,
  autoplay: false,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationTwo
});

const anim3 = lottie.loadAnimation({
  container: document.getElementsByClassName('animation-three')[0], // the dom element that will contain the animation
  renderer: 'svg',
  loop: false,
  autoplay: false,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationThree
});

const exitAnim = lottie.loadAnimation({
  container: document.getElementById('exit-animation'), // the dom element that will contain the animation
  renderer: 'svg',
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationOutro
});




const introAnimMobile = lottie.loadAnimation({
  container: document.getElementById('intro-animation-mobile'), // the dom element that will contain the animation
  renderer: 'svg',
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationIntro
});

const anim1Mobile = lottie.loadAnimation({
  container: document.getElementById('animation-one-mobile'), // the dom element that will contain the animation
  renderer: 'svg',
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationOne
});

const anim2Mobile = lottie.loadAnimation({
  container: document.getElementById('animation-two-mobile'), // the dom element that will contain the animation
  renderer: 'svg',
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationTwo
});

const anim3Mobile = lottie.loadAnimation({
  container: document.getElementById('animation-three-mobile'), // the dom element that will contain the animation
  renderer: 'svg',
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationThree
});

const outroMobile = lottie.loadAnimation({
  container: document.getElementById('animation-outro-mobile'), // the dom element that will contain the animation
  renderer: 'svg',
  loop: true,
  autoplay: true,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  },
  animationData: animationOutro
});

// responsively start and destroy the slider
enquire.register("screen and (min-width: 768px)", {
  match: function() {
    bigSlider()
  },
  unmatch: function() {
    fullpage_api.destroy('all')
    $('#clock').removeClass()
  }
})


// instantiate the slider and freeze scrolling
function bigSlider() {
  let fullPageInstance = new fullpage('#fullpage', {
    licenseKey: 'EAC92CEA-4B144C35-99191641-C456883A',
    navigation: false,
    scrollOverflow: true,
    onLeave: function(origin, destination, direction) {
      slideReset()
      animateClock(destination.index)

    },
    afterRender: function() {
      $('#clock')
        .removeClass('time1 time2 time3 animated')
    }
  });

  fullpage_api.setAllowScrolling(false)
}


// CLOCK ANIMATION FUNCTION
function animateClock(slide) {
  let $clock = $('#clock')

  if ( slide > 0 ) {
    $clock.removeClass('fadeOutDown').addClass('animated fadeInUp ready')
  } 

  switch (slide) {
    // first page
    case 0:
      // are we returning?
      if ( $clock.hasClass('animated') ) {
        $clock
          .removeClass('time1 time2 time3 animated')
          .addClass('fadeOutDown')
      }

    case 1:
      $clock
        .removeClass('time2 time3')
        .addClass('time1')
      break

    case 2:
      $clock
        .removeClass('time1 time3')
        .addClass('time2')
      break
      
    case 3:
      $clock
        .removeClass('time1 time2')
        .addClass('time3')
      break
    
    case 4:
      $clock
        .addClass('the-end')

    default:
      $clock.removeClass('time1 time2 time3 ready')  
  }
}

// pause the slides
function slideReset() {
  anim1.pause()
  anim2.pause()
  anim3.pause()

  setTimeout( () => {
    $('.before, .after').removeClass('animated fadeOutUp fadeInUp')
    $('.before').addClass('fadeInDown')
  }, 250)

  setTimeout( () => {
    anim1.goToAndStop(0)
    anim2.goToAndStop(0)
    anim3.goToAndStop(0)
  }, 750)
}

// animate in the text and play video
function stageTwoGo(animation, el) {
  const thisBefore = $(el).parent().parent().parent().find('.before')
  const thisAfter = $(el).parent().parent().parent().find('.after')

  if ( $(thisAfter).hasClass('fadeInUp') ) {
    fullpage_api.moveSectionDown()
  } else {
    $(thisBefore).removeClass('fadeInUp fadeInDown').addClass('animated fadeOutUp')
    $(thisAfter).removeClass('fadeOutUp fadeOutDown').addClass('animated fadeInUp')  
    animation.play()
    animation.setDirection(1)
  }
}

function stageTwoUnGo(animation, el) {
  const thisBefore = $(el).parent().parent().parent().find('.before')
  const thisAfter = $(el).parent().parent().parent().find('.after')

  if ( $(thisBefore).hasClass('fadeInDown') ) {
    fullpage_api.moveSectionUp()
  } else {
    $(thisAfter).removeClass('fadeInUp fadeInDown').addClass('animated fadeOutDown')
    $(thisBefore).removeClass('fadeOutUp fadeOutDown').addClass('animated fadeInDown')  
    animation.setDirection(-1)
    animation.play()
  }
}


$('.slide-one .next').on('click', function(e) {
  e.preventDefault()
  stageTwoGo(anim1, this)
})

$('.slide-two .next').on('click', function(e) {
  e.preventDefault()
  stageTwoGo(anim2, this)
})

$('.slide-three .next').on('click', function(e) {
  e.preventDefault()
  stageTwoGo(anim3, this)
})

$('.slide-one .prev').on('click', function(e) {
  e.preventDefault()
  stageTwoUnGo(anim1, this)
})
$('.slide-two .prev').on('click', function(e) {
  e.preventDefault()
  stageTwoUnGo(anim2, this)
})
$('.slide-three .prev').on('click', function(e) {
  e.preventDefault()
  stageTwoUnGo(anim3, this)
})



$('.slide-next').on('click', (e) => {
  e.preventDefault()
  fullpage_api.moveSectionDown() 
})



$('.slide-end').on('click', e => {
  e.preventDefault()
  fullpage_api.moveTo(1)
})

$(window).on('load', () => {
  $('#scrim').addClass('be-gone')
})