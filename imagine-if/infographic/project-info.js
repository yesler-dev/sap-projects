module.exports = {
  // this prefaces your file with something unique when you upload.
  uniqueID: 'abcdefg',
  // name of the instance that files default their paths to... default is google's old one
  mktoInstance: 'https://lp.google-mkto.com/rs/248-TPC-286/images',
  // your name so we know how did so great!
  currentEditor: '@yourname',
}