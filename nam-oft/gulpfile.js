// Include gulp
var gulp = require('gulp');

// Include plugins
var fs             = require('fs');
var sass           = require('gulp-sass');
var concat         = require('gulp-concat');
var uglify         = require('gulp-uglify');
var livereload     = require('gulp-livereload');
var connect        = require('gulp-connect');
var plumber        = require('gulp-plumber');
var postcss        = require('gulp-postcss');
var autoprefixer   = require('autoprefixer');
var rename         = require('gulp-rename');
var replace        = require('gulp-replace');
var cssmin         = require('gulp-cssmin');
var concatCss      = require('gulp-concat-css');
var nunjucksRender = require('gulp-nunjucks-render');
var gulpif         = require('gulp-if');
var data           = require('gulp-data');
var browserSync    = require('browser-sync').create();


var paths = {
    html: {
        src: './index.html',
        dest: './'
    }
}

function template() {
    return gulp.src(paths.html.src)
        .pipe(gulp.dest(paths.html.dest))
        .pipe(browserSync.stream());
}// end style()

function reload() {
    browserSync.reload();
}// end reload()

function watch() {
    browserSync.init({
        server: {
            baseDir: './'
        }
    })
    gulp.watch(paths.html.src).on('change', reload);

}// end watch()

gulp.task('default', gulp.series(watch));
















