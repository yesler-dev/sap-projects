require("dotenv").config()
const currentEditor = process.env.EDITOR
const mktoUrl = "https://na-sj24.marketo.com/rs/808-GJW-314/images"
const uniqueKey = "1a2b3c" // // set to something unique. keep it alphanumerica

module.exports = {
  mktoUrl,
  uniqueKey,
  currentEditor,
}
