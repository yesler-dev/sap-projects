'use strict'

const gulp = require("gulp")
const browserSync = require('browser-sync').create()
const reload = browserSync.reload
const { series, parallel } = require("gulp")
const del = require('del')
const nunjucksRender = require("gulp-nunjucks-render")
const replace = require("gulp-replace")
const moment = require('moment')
const { currentEditor, mktoUrl, uniqueKey } = require("./setup.js")

if (typeof currentEditor === "undefined") {
  console.log(
    `\n\n 
    😡
    Be sure to create a .env file in the project root with this syntax:
    EDITOR=yourname
    `,
  )
  throw new console.error()
} else {
  console.log(
    `\nHello ${currentEditor}!\nGLHF and hopefully no (╯°□°)╯︵ ┻━┻\n`,
  )
}

const paths = {
  dist: './dist',
  html: {
    watch: "./dist/**/*.html",
    dest: "./dist"
  },
  img: {
    watch: './src/images/*',
    dest: './dist/images'
  },
  nunjucks: {
    src: "./src/templates/pages/**/*.njk",
    tmpl: "./src/templates/",
    watch: "./src/templates/**/*.njk",
    dest: "./dist"
  }
}





function nunjucks() {
  return gulp.src(paths.nunjucks.src)
    .pipe(nunjucksRender({
      path: [paths.nunjucks.tmpl],
      envOptions: {
        /*
        tags: {
          variableStart: "<$",
          variableEnd: "$>",
        },
        */
      },
    }))
    .pipe(gulp.dest(paths.nunjucks.dest))
}

function watch() {
  browserSync.init({
    server: {
      baseDir: "./dist",
    }
  })
  gulp.watch(paths.img.watch, images).on('change', reload)
  gulp.watch(paths.nunjucks.watch, nunjucks)
  gulp.watch(paths.html.watch).on('change', reload)
}

function images() {
  return gulp.src(paths.img.watch)
    // .pipe(imagemin())
    .pipe(gulp.dest(paths.img.dest))
}


function meta() {
  const timestamp = moment() 
  return gulp.src(['dist/**/*.html'])
    .pipe(replace('@meta', `Edited by ${currentEditor} on ${timestamp}.`))
    .pipe(gulp.dest('dist/'))
}


function clearDist() {
  return del(['dist'])
}


exports.images = images
exports.nunjucks = nunjucks
exports.watch = watch
exports.meta = meta
exports.clearDist = clearDist
exports.build = series(nunjucks, meta)
exports.default = parallel(nunjucks, watch)
